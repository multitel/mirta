<?php
/*
 * Copyright 2018-2019 MultiTEL LLC
 * @ Website    : https://www.multitel.net
 * @ Released	: 2019/04/27

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

 */

require_once 'include/general.inc.php';
check_auth();
require_once 'include/db.inc.php';
require_once 'include/multitel.config.php';

$number = (!empty($_POST['number']) ? mysql_real_escape_string(trim($_POST['number'])) : '');
$prefix_uuid = (!empty($_POST['prefix_uuid']) ? mysql_real_escape_string(trim($_POST['prefix_uuid'])) : '');
$price_uuid = (!empty($_POST['price_uuid']) ? mysql_real_escape_string(trim($_POST['price_uuid'])) : '');
$restrictions_obj = (!empty($_POST['restrictions']) ? json_decode($_POST['restrictions']) : array());
$restrictions_html = (!empty($restrictions_obj->restrictions) ? nl2br($restrictions_obj->restrictions) : '');
$features_obj = (!empty($_POST['features']) ? json_decode($_POST['features']) : array());

//$tenantid = ($_SESSION['selectedTenant']);
$tenantid = 0;

$getpriceinfo_obj = getpriceinfo($tenantid, $price_uuid);

$SERVERIPADDRESS = get_multitel_setting($tenantid, 'SERVERIPADDRESS');
$uri = '{E164}@'.$SERVERIPADDRESS;
$uri2 = $uri;
$uri3 = $uri;

$needs_registration_html = "";
if(!empty($restrictions_obj->needs_registration)){
	$needs_registration_html = "
		<script>
		$(document).ready(function(){
			$('#buy-sidebar').addClass('hidden');
			$('#buy-main').addClass('hidden');
			$('#registration').removeClass('hidden');
		});
	</script>
	<div class='content-wrapper hidden' id='registration'>
		<div class='row'>
			<div class='panel panel-flat'>
				<div class='panel-body' id='contentRestriction'>
					".$restrictions_html."
				</div>
			</div>
			<div class='panel panel-flat'>
				<div class='panel-body'>
					<div class='col-md-12'>
						<label>Please read the above requirements carefully and confirm below if you are able to provide the required documents.</label>
						<br><br>
						<button type='button' class='btn btn-danger' data-dismiss='modal'>I am NOT able to provide these documents</button>
						<button type='button' class='btn btn-success' onclick='javascript: registrationConfirmed();'>I am able to provide these documents</button>
						<br><br>
						If unsure, please DO NOT complete the purchase. Please email us at support@multitel.net if in doubt.
					</div>
	
					<script type='text/javascript'>
						$('#btnSubmitPurchase').addClass('hidden');
						function registrationConfirmed() { 
							$('#registration').addClass('hidden');
							$('#buy-sidebar').removeClass('hidden');
							$('#buy-main').removeClass('hidden');
							$('#btnSubmitPurchase').removeClass('hidden');	
						}
					</script>
				</div>
			</div>
		</div>
	</div>
	";
}
echo $needs_registration_html;

$quantity = 1;
$show_pricing_text_tab = false;
$show_pricing_mms_tab = false;
$feat = "";
if(!empty($features_obj)){
	foreach ($features_obj as $feature) {
		switch($feature) {
			case "voice": 
				$feat.= "<li class='fa fa-mobile mr-5' alt='Voice Enabled' title='Voice Enabled'></li>"; 
				break;
			case "t38": 
				$feat.= "<li class='fa fa-newspaper-o mr-5' alt='T.38 Enabled' title='T.38 Enabled'></li>";
				break;
			case "sms": 
				$feat.= "<li class='fa fa-envelope-o mr-5' alt='SMS enabled' title='SMS Enabled'></li>";
				break;
			case "mms": 
				$feat.= "<li class='fa fa-file-video-o mr-5' alt='MMS Enabled' title='MMS Enabled'></li>";
				break;
		}
	}
}

$setup_price_html = "";
if(!empty($getpriceinfo_obj->setup_price)){
	$setup_price_html = "
		<tr>
			<td>Setup</td>
			<td id='setup_price'>$".round($getpriceinfo_obj->setup_price,6)."</td>
			<td>NRC</td>
		</tr>
	";
}
$monthly_price_html = "";
if(!empty($getpriceinfo_obj->monthly_price)){
	$monthly_price_html = "
		<tr>
			<td>DID&nbsp;Rental</td>
			<td id='monthly_price'>$".round($getpriceinfo_obj->monthly_price,6)."</td>
			<td>Monthly</td>
		</tr>
	";
}
$yearly_price_html = "";
if(!empty($getpriceinfo_obj->yearly_price)){
	$yearly_price_html = "
		<tr>
			<td>DID&nbsp;Rental</td>
			<td id='monthly_price'>$".round($getpriceinfo_obj->yearly_price,6)."</td>
			<td>Yearly</td>
		</tr>
	";
}

$per_minute_html = "";
if(isset($getpriceinfo_obj->per_minute)){
	$per_minute_html = "
		<tr>
			<td>Inbound&nbsp;cost</td>
			<td>$".round($getpriceinfo_obj->per_minute,6)."/min</td>
		</tr>
	";
}
$feature_html = "
<div class='row'>
<div class='col-md-4'>
<div class='sidebar sidebar-secondary sidebar-default border-default border-radius-5' id='buy-sidebar'>
	<div class='sidebar-content'>
		<div class='sidebar-category'>
			<div class='category-title'>
				<span>Features</span>
				<ul class='icons-list'>
					".$feat."
				</ul>
			</div>
			<div class='category-content'>
				<label class='text-semibold'>
					Number costs:
				</label>
				<div class='tabbable tab-content-bordered'>
					<ul class='nav nav-xs nav-tabs nav-tabs-highlight nav-justified nav-tabs-bottom'>
						<li class='active'>
							<a href='#pricing-costs' data-toggle='tab'>
								Costs
							</a>
						</li>
						<li>
							<a href='#pricing-minutes' data-toggle='tab'>
								Minutes
							</a>
						</li>
						<li ".(!empty($show_pricing_text_tab) ? '' : 'style="display:none"').">
							<a href='#pricing-texting' data-toggle='tab'>
								Texting
							</a>
						</li>
					</ul>
					<div class='tab-content'>
						<div class='tab-pane active' id='pricing-costs'>
							<div class='table-responsive'>
								<table class='table table-xxs' style='width: 270px;'>
									<thead>
										<th width='60%'>Service</th>
										<th>Cost</th>
										<th>When</th>
									</thead>
									<tbody>
										".$setup_price_html."
										".$monthly_price_html."
										".$yearly_price_html."
									</tbody>
								</table>
							</div>
						</div>
						<div class='tab-pane' id='pricing-minutes'>
							<div class='table-responsive'>
								<table class='table table-xxs' style='width: 270px;'>
									<thead>
										<th width='60%'>Service</th>
										<th width='40%'>Info</th>
									</thead>
									<tbody>
										".$per_minute_html."
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- cost explained modal -->
					<br>
					<input type='hidden' name='quantity' value='1' />
				</div>
			</div>
		</div>
	</div>
";

echo $feature_html;

$feature_html = "
	</div>
</div>
";
echo $feature_html;

$cnam=0;
$rent_number_input_html = "
<div class='col-md-8'>
	<div class='content-wrapper border-default border-radius-5' id='buy-main'>
		<!-- Basic responsive configuration -->
		<div class='panel panel-flat no-box-shadow'>
			<div class='panel-body'>
				<div class='tabbable tab-content-bordered'>
					<ul class='nav nav-xs nav-tabs nav-tabs-solid nav-tabs-component'>
						<li class='active'>
							<a href='#tab_number_setting' data-toggle='tab'>
								Number settings
							</a>
						</li>
					</ul>
						<div class='tab-content'>
							<div class='tab-pane active' id='tab_number_setting'>
								<!-- start number settings -->
								<div class='panel-body'>
									<div class='col-md-6'>
										<div class='content-group-sm'>
											<label class='text-semibold'>Description</label>
											<input type='text' class='form-control maxlength' maxlength='20' placeholder='Number description' id='description' name='description' value=''>
											<span class='help-block'>Eg.: 'helpdesk&nbsp;line'</span>
											<input type='hidden' name='number' value='".(!empty($number) ? $number : '')."'>
											<input type='hidden' name='overflow' id='input_overflow' value='".(!empty($overflow) ? $overflow : 1)."' />
											<input type='hidden' name='autorenew' id='input_autorenew' value='".(!empty($autorenew) ? $autorenew : 1)."' />
											<input type='hidden' name='price_uuid' id='input_price_uuid' value='".(!empty($price_uuid) ? $price_uuid : 1)."' />
											
										</div>
									</div>
									<div class='col-md-6'>
										<div class='content-group-sm'>
											<label class='text-semibold'>CNAM</label>
											<select class='form-control select' placeholder='CNAM Enabled' id='cnam' name='cnam'>
												<option ".(isset($cnam) && $cnam == 1 ? 'selected="selected"' : '')." value='1'>Yes</option>
												<option ".(isset($cnam) && $cnam == 0 ? 'selected="selected"' : '')." value='0'>No</option>
											</select>
											<span class='help-block'>CallerID name show: $".(!empty($cnam_price) ? round($cnam_price, 4) : 0)."/call</span>
										</div>
									</div>
									<div class='col-md-6'>
										<input type='hidden' name='forward_destination' value='1'>
										<input type='hidden' name='uri' id='uri' value='".(!empty($uri) ? $uri : '')."' class='form-control'>
										<input type='hidden' name='uri2' id='uri2' value='".(!empty($uri2) ? $uri2 : '')."' class='form-control'>
										<input type='hidden' name='uri3' id='uri3' value='".(!empty($uri3) ? $uri3 : '')."' class='form-control'>
									</div>
								</div>
								<!-- end number settings -->
								<input type='hidden' name='ring_type' value='1'>
								<input type='hidden' name='force_codec' value=''>
								<input type='hidden' name='force_dtmf' value=''>
							</div>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
";
echo $rent_number_input_html;
?>
