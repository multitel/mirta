<?php
/*
 * Copyright 2018-2019 MultiTEL LLC
 * @ Website    : https://www.multitel.net
 * @ Released	: 2019/04/27

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

 */

require_once 'include/general.inc.php';
check_auth();
require_once 'include/multitel.config.php';

if(!empty($_SESSION['hasMenuAdmin']) && $_SESSION['hasMenuAdmin'] == 'yes'){
        $has_access = true;
}
if(!$has_access){
        $smarty->display('forbidden.tpl');
        exit();
}

$status = '';
$message = '';
$tenantid = 0;

if(!empty($_POST['save_setting'])){
	$custom_validation_error = false;
	
	$MULTITELUSER = (!empty($_POST['MULTITELUSER']) ? mysql_real_escape_string(trim($_POST['MULTITELUSER'])) : '');
	$MULTITELPASSWORD = (!empty($_POST['MULTITELPASSWORD']) ? mysql_real_escape_string(trim($_POST['MULTITELPASSWORD'])) : '');
	$SERVERIPADDRESS = (!empty($_POST['SERVERIPADDRESS']) ? mysql_real_escape_string(trim($_POST['SERVERIPADDRESS'])) : '');
	$MULTITELDIDMAXSETUPFEE = (!empty($_POST['MULTITELDIDMAXSETUPFEE']) ? mysql_real_escape_string(trim($_POST['MULTITELDIDMAXSETUPFEE'])) : '');
	$MULTITELDIDMAXMONTHLYFEE = (!empty($_POST['MULTITELDIDMAXMONTHLYFEE']) ? mysql_real_escape_string(trim($_POST['MULTITELDIDMAXMONTHLYFEE'])) : '');
	$MULTITELDIDMAXMINUTEFEE = (!empty($_POST['MULTITELDIDMAXMINUTEFEE']) ? mysql_real_escape_string(trim($_POST['MULTITELDIDMAXMINUTEFEE'])) : '');
	$MULTITELSETUPMARKUP = (!empty($_POST['MULTITELSETUPMARKUP']) ? mysql_real_escape_string(trim($_POST['MULTITELSETUPMARKUP'])) : '');
	$MULTITELMONTHLYMARKUP = (!empty($_POST['MULTITELMONTHLYMARKUP']) ? mysql_real_escape_string(trim($_POST['MULTITELMONTHLYMARKUP'])) : '');
	$MULTITELYEARLYMARKUP = (!empty($_POST['MULTITELYEARLYMARKUP']) ? mysql_real_escape_string(trim($_POST['MULTITELYEARLYMARKUP'])) : '');
	$MULTITELPERMINUTEMARKUP = (!empty($_POST['MULTITELPERMINUTEMARKUP']) ? mysql_real_escape_string(trim($_POST['MULTITELPERMINUTEMARKUP'])) : '');
	$MULTITELSALEPRICEE911 = (!empty($_POST['MULTITELSALEPRICEE911']) ? mysql_real_escape_string(trim($_POST['MULTITELSALEPRICEE911'])) : '');
	$MULTITELEMAILNOTIFICATIONADDRESS = (!empty($_POST['MULTITELEMAILNOTIFICATIONADDRESS']) ? mysql_real_escape_string(trim($_POST['MULTITELEMAILNOTIFICATIONADDRESS'])) : '');
	
	
	if(!$custom_validation_error){
		update_multitel_setting($tenantid, 'MULTITELUSER', $MULTITELUSER);
		update_multitel_setting($tenantid, 'MULTITELPASSWORD', $MULTITELPASSWORD);
		update_multitel_setting($tenantid, 'SERVERIPADDRESS', $SERVERIPADDRESS);
		update_multitel_setting($tenantid, 'MULTITELDIDMAXSETUPFEE', $MULTITELDIDMAXSETUPFEE);
		update_multitel_setting($tenantid, 'MULTITELDIDMAXMONTHLYFEE', $MULTITELDIDMAXMONTHLYFEE);
		update_multitel_setting($tenantid, 'MULTITELDIDMAXMINUTEFEE', $MULTITELDIDMAXMINUTEFEE);
		update_multitel_setting($tenantid, 'MULTITELSETUPMARKUP', $MULTITELSETUPMARKUP);
		update_multitel_setting($tenantid, 'MULTITELMONTHLYMARKUP', $MULTITELMONTHLYMARKUP);
		update_multitel_setting($tenantid, 'MULTITELYEARLYMARKUP', $MULTITELYEARLYMARKUP);
		update_multitel_setting($tenantid, 'MULTITELPERMINUTEMARKUP', $MULTITELPERMINUTEMARKUP);
		update_multitel_setting($tenantid, 'MULTITELSALEPRICEE911', $MULTITELSALEPRICEE911);
		update_multitel_setting($tenantid, 'MULTITELEMAILNOTIFICATIONADDRESS', $MULTITELEMAILNOTIFICATIONADDRESS);
		
		$status = 'success';
		$message = 'Successfully Saved';
	}
	$message_arr = array(
		'status' => $status,
		'message' => $message
	);
	echo json_encode($message_arr);
	exit;
}
$smarty->assign('pagetitle', "MultiTEL Settings");
$smarty->assign('MULTITELUSER', get_multitel_setting($tenantid, 'MULTITELUSER'));
$smarty->assign('MULTITELPASSWORD', get_multitel_setting($tenantid, 'MULTITELPASSWORD'));
$smarty->assign('SERVERIPADDRESS', get_multitel_setting($tenantid, 'SERVERIPADDRESS'));
$smarty->assign('MULTITELDIDMAXSETUPFEE', get_multitel_setting($tenantid, 'MULTITELDIDMAXSETUPFEE'));
$smarty->assign('MULTITELDIDMAXMONTHLYFEE', get_multitel_setting($tenantid, 'MULTITELDIDMAXMONTHLYFEE'));
$smarty->assign('MULTITELDIDMAXMINUTEFEE', get_multitel_setting($tenantid, 'MULTITELDIDMAXMINUTEFEE'));
$smarty->assign('MULTITELSETUPMARKUP', get_multitel_setting($tenantid, 'MULTITELSETUPMARKUP'));
$smarty->assign('MULTITELMONTHLYMARKUP', get_multitel_setting($tenantid, 'MULTITELMONTHLYMARKUP'));
$smarty->assign('MULTITELYEARLYMARKUP', get_multitel_setting($tenantid, 'MULTITELYEARLYMARKUP'));
$smarty->assign('MULTITELPERMINUTEMARKUP', get_multitel_setting($tenantid, 'MULTITELPERMINUTEMARKUP'));
$smarty->assign('MULTITELSALEPRICEE911', get_multitel_setting($tenantid, 'MULTITELSALEPRICEE911'));
$smarty->assign('MULTITELEMAILNOTIFICATIONADDRESS', get_multitel_setting($tenantid, 'MULTITELEMAILNOTIFICATIONADDRESS'));

$smarty->display('multitelsettings.tpl');

?>
