<?php
/*
 * Copyright 2018-2019 MultiTEL LLC
 * @ Website    : https://www.multitel.net
 * @ Released	: 2019/04/27

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

 */

require_once 'include/general.inc.php';
check_auth();
$has_access = false;
if(!empty($_SESSION['hasMenuAdmin']) && $_SESSION['hasMenuAdmin'] == 'yes'){
	$has_access = true;
}
if(!$has_access){
	$smarty->display('forbidden.tpl');
	exit();
}
require_once 'include/multitel.config.php';

$te_tenants_sql = "SELECT * FROM te_tenants order by te_name asc";
$te_tenants_query = mysql_query($te_tenants_sql) or die(mysql_error());
$te_tenants_arr = array();
while($te_tenants_row = mysql_fetch_array($te_tenants_query, MYSQL_ASSOC)){
	$te_tenants_arr[] = (object)$te_tenants_row;
}
$smarty->assign('pagetitle', "Admin E911");
$smarty->assign('te_tenants_arr', $te_tenants_arr);
$smarty->display('admin911.tpl');

?>
