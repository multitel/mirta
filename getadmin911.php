<?php
/*
 * Copyright 2018-2019 MultiTEL LLC
 * @ Website    : https://www.multitel.net
 * @ Released	: 2019/04/27

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

 */

require_once 'include/general.inc.php';
check_auth();

require_once 'include/multitel.config.php';

$te_id = mysql_real_escape_string(!empty($_GET['te_id']) ? $_GET['te_id'] : '');
$sn = mysql_real_escape_string(!empty($_GET['sn']) ? $_GET['sn'] : '');

$tenantid = 0;
$multitel_api_url = get_multitel_api_url();
$multitel_username = get_multitel_setting($tenantid, 'MULTITELUSER');
$multitel_password = get_multitel_setting($tenantid, 'MULTITELPASSWORD');
$version_api = get_version_api($tenantid);

$url = $multitel_api_url.$version_api.'/tn911inventory';
$curlopt = array(
	CURLOPT_RETURNTRANSFER => true,   // return web page
	CURLOPT_HEADER         => false,  // don't return headers
	CURLOPT_FOLLOWLOCATION => true,   // follow redirects
	CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
	CURLOPT_ENCODING       => "",     // handle compressed
	CURLOPT_USERAGENT      => "MirtaPBX", // name of client
	CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
	CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
	CURLOPT_TIMEOUT        => 120,    // time-out on response
	CURLOPT_POST           => true,
	CURLOPT_HTTPAUTH       => CURLAUTH_BASIC,
	CURLOPT_USERPWD        => "$multitel_username:$multitel_password", 
);

$ch = curl_init($url);
curl_setopt_array($ch, $curlopt);
$content  = curl_exec($ch);
curl_close($ch);
$inventory_result_obj = json_decode($content);

$di_dids_sql = "SELECT * FROM di_dids";
if(!empty($te_id)){
	$di_dids_sql .= " WHERE di_te_id = '".$te_id."'";
}
$di_dids_sql .= " order by di_te_id asc";
$di_dids_query = mysql_query($di_dids_sql) or die(mysql_error());
$di_dids_arr = array();
$di_dids_number_arr = array();
while($di_dids_row = mysql_fetch_array($di_dids_query, MYSQL_ASSOC)){
	if(!empty($di_dids_row['di_number'])){
		$di_dids_arr[$di_dids_row['di_number']] = (object)$di_dids_row;
		$di_dids_number_arr []= $di_dids_row['di_number'];
	}
}
$inventory_local_arr = array();
$inventory_all_arr = array();
if(!empty($inventory_result_obj->response)){
	foreach($inventory_result_obj->response as $inventory_obj){
		$inventory_obj->te_id = (!empty($di_dids_arr[$inventory_obj->tn]->di_te_id) ? $di_dids_arr[$inventory_obj->tn]->di_te_id : 0);
		if(!empty($inventory_obj->tn)){
			if(in_array($inventory_obj->tn, $di_dids_number_arr)){
				$inventory_local_arr [] = $inventory_obj;
			} else {
				if(empty($te_id)){
					$inventory_all_arr [] = $inventory_obj;
				}
			}
		}
	}
}

$inventory_arr = array();
if($sn == 'all'){
	$inventory_arr = array_merge($inventory_local_arr, $inventory_all_arr);
} else {
	$inventory_arr = $inventory_local_arr;
}

header('Content-Type: text/plain');

$getadmin911_html = "";
if(!empty($inventory_arr)){
	foreach($inventory_arr as $inventory_obj) {
		$te_tenants_obj = array();
		if(!empty($inventory_obj->te_id)){
			$te_tenants_sql = "SELECT * FROM te_tenants WHERE te_id='".$inventory_obj->te_id."'";
			$te_tenants_query = mysql_query($te_tenants_sql) or die(mysql_error());
			$te_tenants_row = mysql_fetch_array($te_tenants_query, MYSQL_ASSOC);
			$te_tenants_obj = (object)$te_tenants_row;
		}
		$inventory_number = (!empty($inventory_obj->tn) ? $inventory_obj->tn : '');
		$getadmin911_html .= "
			<tr id='tr_adminnumber_".$inventory_number."'>
				<td>
					".(!empty($te_tenants_obj->te_name) ? $te_tenants_obj->te_name : '').(!empty($te_tenants_obj->te_code) ? ' - '.$te_tenants_obj->te_code : '')."
				</td>
				<td>
					<span class='label label-success'>".$inventory_number."</span>
				</td>
				<td>
					".(!empty($inventory_obj->name) ? $inventory_obj->name : '')."
				</td>
				<td>
					".(!empty($inventory_obj->address) ? $inventory_obj->address : '')."
				</td>
				<td>
					".(!empty($inventory_obj->state) ? $inventory_obj->state : '')."
				</td>
				<td>
					".(!empty($inventory_obj->city) ? $inventory_obj->city : '')."
				</td>
				<td>
					".(!empty($inventory_obj->postalCode) ? $inventory_obj->postalCode : '')."
				</td>
				<td>
					<li class='cursor-pointer release_number btn btn-default btn-xs mb-5' alt='Release number' title='Release numbers' data-number='".$inventory_number."'>Release numbers</li>
				</td>
			</tr>
		";
	}
}
?>
