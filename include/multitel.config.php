<?php
/*
 * Copyright 2018-2019 MultiTEL LLC
 * @ Website    : https://www.multitel.net
 * @ Released   : 2019/04/27

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

 */

require_once 'include/general.inc.php';

$multitel_api_username = "";
$multitel_api_password = "";


$multitel_api_url = get_multitel_api_url();

//$tenantid = ($_SESSION['selectedTenant']);
$tenantid = 0;

$multitel_username = get_multitel_setting($tenantid, 'MULTITELUSER');
$multitel_password = get_multitel_setting($tenantid, 'MULTITELPASSWORD');

function getSetupPrice($setup_price){
	$tenantid = 0;
	$setup_markup = get_multitel_setting($tenantid, 'MULTITELSETUPMARKUP');
	if(empty($setup_markup)){
		$setup_markup = 0;
	}
	$return_price = (!empty($setup_markup) ? ($setup_markup*$setup_price)/100 : 0)+$setup_price;
	return $return_price;
}
function getMonthlyPrice($monthly_price){
	$tenantid = 0;
	$monthly_markup = get_multitel_setting($tenantid, 'MULTITELMONTHLYMARKUP');
	if(empty($monthly_markup)){
		$monthly_markup = 0;
	}
	$return_price = (!empty($monthly_markup) ? ($monthly_markup*$monthly_price)/100 : 0)+$monthly_price;
	return $return_price;
}
function getYearlyPrice($yearly_price){
	$tenantid = 0;
	$yearly_markup = get_multitel_setting($tenantid, 'MULTITELYEARLYMARKUP');
	if(empty($yearly_markup)){
		$yearly_markup = 0;
	}
	$return_price = $yearly_price+(!empty($yearly_markup) ? ($yearly_markup*$yearly_price)/100 : 0);
	return $return_price;
}
function getPerMinutePrice($per_minute){
	$tenantid = 0;
	$per_minute_markup = get_multitel_setting($tenantid, 'MULTITELPERMINUTEMARKUP');
	if(empty($per_minute_markup)){
		$per_minute_markup = 0;
	}
	$return_price = $per_minute+(!empty($per_minute_markup) ? ($per_minute_markup*$per_minute)/100 : 0);
	return $return_price;
}

function listState(){
	$state_arr[] = (object)array(
		'name' => 'Alabama',
		'state_iso' => 'AL',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Alaska',
		'state_iso' => 'AK',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Alberta',
		'state_iso' => 'AB',
		'country_iso' => 'CA',
	);
	$state_arr[] = (object)array(
		'name' => 'American Samoa',
		'state_iso' => 'AS',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Arizona',
		'state_iso' => 'AZ',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Arkansas',
		'state_iso' => 'AR',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'British Columbia',
		'state_iso' => 'BC',
		'country_iso' => 'CA',
	);
	$state_arr[] = (object)array(
		'name' => 'California',
		'state_iso' => 'CA',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Colorado',
		'state_iso' => 'CO',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Connecticut',
		'state_iso' => 'CT',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Delaware',
		'state_iso' => 'DE',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'District of Columbia',
		'state_iso' => 'DC',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Florida',
		'state_iso' => 'FL',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Georgia',
		'state_iso' => 'GA',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Guam',
		'state_iso' => 'GU',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Hawaii',
		'state_iso' => 'HI',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Idaho',
		'state_iso' => 'ID',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Illinois',
		'state_iso' => 'IL',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Indiana',
		'state_iso' => 'IN',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Iowa',
		'state_iso' => 'IA',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Kansas',
		'state_iso' => 'KS',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Kentucky',
		'state_iso' => 'KY',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Louisiana',
		'state_iso' => 'LA',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Maine',
		'state_iso' => 'ME',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Manitoba',
		'state_iso' => 'MB',
		'country_iso' => 'CA',
	);
	$state_arr[] = (object)array(
		'name' => 'Maryland',
		'state_iso' => 'MD',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Massachusetts',
		'state_iso' => 'MA',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Michigan',
		'state_iso' => 'MI',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Minnesota',
		'state_iso' => 'MN',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Mississippi',
		'state_iso' => 'MS',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Missouri',
		'state_iso' => 'MO',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Montana',
		'state_iso' => 'MT',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Nebraska',
		'state_iso' => 'NE',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Nevada',
		'state_iso' => 'NV',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'New Brunswick',
		'state_iso' => 'NB',
		'country_iso' => 'CA',
	);
	$state_arr[] = (object)array(
		'name' => 'New Hampshire',
		'state_iso' => 'NH',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'New Jersey',
		'state_iso' => 'NJ',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'New Mexico',
		'state_iso' => 'NM',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'New York',
		'state_iso' => 'NY',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Newfoundland and Labrador',
		'state_iso' => 'NL',
		'country_iso' => 'CA',
	);
	$state_arr[] = (object)array(
		'name' => 'North Carolina',
		'state_iso' => 'NC',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'North Dakota',
		'state_iso' => 'ND',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Northern Mariana Islands',
		'state_iso' => 'MP',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Northwest Territories',
		'state_iso' => 'NT',
		'country_iso' => 'CA',
	);
	$state_arr[] = (object)array(
		'name' => 'Nova Scotia',
		'state_iso' => 'NS',
		'country_iso' => 'CA',
	);
	$state_arr[] = (object)array(
		'name' => 'Nunavut',
		'state_iso' => 'NU',
		'country_iso' => 'CA',
	);
	$state_arr[] = (object)array(
		'name' => 'Ohio',
		'state_iso' => 'OH',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Oklahoma',
		'state_iso' => 'OK',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Ontario',
		'state_iso' => 'ON',
		'country_iso' => 'CA',
	);
	$state_arr[] = (object)array(
		'name' => 'Oregon',
		'state_iso' => 'OR',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Pennsylvania',
		'state_iso' => 'PA',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Prince Edward Island',
		'state_iso' => 'PE',
		'country_iso' => 'CA',
	);
	$state_arr[] = (object)array(
		'name' => 'Puerto Rico',
		'state_iso' => 'PR',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Quebec',
		'state_iso' => 'QC',
		'country_iso' => 'CA',
	);
	$state_arr[] = (object)array(
		'name' => 'Rhode Island',
		'state_iso' => 'RI',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Saskatchewan',
		'state_iso' => 'SK',
		'country_iso' => 'CA',
	);
	$state_arr[] = (object)array(
		'name' => 'South Carolina',
		'state_iso' => 'SC',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'South Dakota',
		'state_iso' => 'SD',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Tennessee',
		'state_iso' => 'TN',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Texas',
		'state_iso' => 'TX',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'U.S. Virgin Islands',
		'state_iso' => 'VI',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Utah',
		'state_iso' => 'UT',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Vermont',
		'state_iso' => 'VT',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Virginia',
		'state_iso' => 'VA',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Washington',
		'state_iso' => 'WA',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'West Virginia',
		'state_iso' => 'WV',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Wisconsin',
		'state_iso' => 'WI',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Wyoming',
		'state_iso' => 'WY',
		'country_iso' => 'US',
	);
	$state_arr[] = (object)array(
		'name' => 'Yukon',
		'state_iso' => 'YT',
		'country_iso' => 'CA',
	);
	return $state_arr;
}

function update_multitel_setting($tenantid, $key, $value) {
	$key = preg_replace('|[^a-z0-9_]|i', '', $key);
	if (is_string($value)) $value = stripslashes($value);
	
	$se_settings_sql = "SELECT * FROM se_settings WHERE se_te_id='".$tenantid."' AND se_code='".$key."'";
	$se_settings_query = mysql_query($se_settings_sql) or die(mysql_error());
	$se_settings_row = mysql_fetch_array($se_settings_query, MYSQL_ASSOC);
	
	if (empty($value)) {
		if (!empty($se_settings_row)) {
			$se_settings_delete_sql = "DELETE FROM se_settings WHERE se_te_id='".$tenantid."' AND se_code='".$key."'";
			$se_settings_delete_query = mysql_query($se_settings_delete_sql) or die(mysql_error());
		}
	} else {
		if(!empty($se_settings_row)){
			$se_settings_update_sql = "UPDATE se_settings SET se_value='".$value."' WHERE se_te_id='".$tenantid."' AND se_code='".$key."'";
			$se_settings_update_query = mysql_query($se_settings_update_sql) or die(mysql_error());
		} else {
			$se_settings_insert_sql = "
				INSERT INTO se_settings (se_te_id, se_code, se_value)
				VALUES ('".$tenantid."', '".$key."', '".$value."');
			";
			$se_settings_insert_query = mysql_query($se_settings_insert_sql) or die(mysql_error());
		}
	}
	return true;
}

function get_multitel_setting($tenantid, $key) {
	$key = preg_replace('|[^a-z0-9_]|i', '', $key);
	if (is_string($value)) $value = stripslashes($value);
	
	$se_settings_sql = "SELECT * FROM se_settings WHERE se_te_id='".$tenantid."' AND se_code='".$key."'";
	$se_settings_query = mysql_query($se_settings_sql) or die(mysql_error());
	$se_settings_row = mysql_fetch_array($se_settings_query, MYSQL_ASSOC);
	
	return (!empty($se_settings_row['se_value']) ? $se_settings_row['se_value'] : '');
}

function getpriceinfo($tenantid, $price_uuid){
	$multitel_username = get_multitel_setting($tenantid, 'MULTITELUSER');
	$multitel_password = get_multitel_setting($tenantid, 'MULTITELPASSWORD');
	$multitel_api_url = get_multitel_api_url();
	$version_api = get_version_api($tenantid);
	//$url = $multitel_api_url.'v3/getpriceinfo/'.$price_uuid;
	$url = $multitel_api_url.$version_api.'/getpriceinfo/'.$price_uuid;
	
	$curlopt = array(
		CURLOPT_RETURNTRANSFER => true,   // return web page
		CURLOPT_HEADER         => false,  // don't return headers
		CURLOPT_FOLLOWLOCATION => true,   // follow redirects
		CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
		CURLOPT_ENCODING       => "",     // handle compressed
		CURLOPT_USERAGENT      => "MirtaPBX", // name of client
		CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
		CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
		CURLOPT_TIMEOUT        => 120,    // time-out on response
		CURLOPT_POST           => true,
		CURLOPT_HTTPAUTH       => CURLAUTH_BASIC,
		CURLOPT_USERPWD        => "$multitel_username:$multitel_password", 
	);
	
	$ch = curl_init($url);
	curl_setopt_array($ch, $curlopt);
	$content  = curl_exec($ch);
	curl_close($ch);
	$getpriceinfo_result_obj = json_decode($content);
	
	$getpriceinfo_obj = (!empty($getpriceinfo_result_obj->response[0]) ? $getpriceinfo_result_obj->response[0] : '');
	
	$price_info_arr = (object)array(
		'per_minute' => getPerMinutePrice((!empty($getpriceinfo_obj->per_minute) ? $getpriceinfo_obj->per_minute : 0)),
		'free_minutes' => (!empty($getpriceinfo_obj->free_minutes) ? $getpriceinfo_obj->free_minutes : 0),
		'setup_price' => getSetupPrice((!empty($getpriceinfo_obj->setup_price) ? $getpriceinfo_obj->setup_price : 0)),
		'monthly_price' => getMonthlyPrice((!empty($getpriceinfo_obj->monthly_price) ? $getpriceinfo_obj->monthly_price : 0)),
		'channels' => (!empty($getpriceinfo_obj->channels) ? $getpriceinfo_obj->channels : 0),
		'provider_id' => (!empty($getpriceinfo_obj->provider_id) ? $getpriceinfo_obj->provider_id : 0),
		'orig_price' => (!empty($getpriceinfo_obj->orig_price) ? $getpriceinfo_obj->orig_price : 0),
		'yearly_price' => getYearlyPrice((!empty($getpriceinfo_obj->yearly_price) ? $getpriceinfo_obj->yearly_price : 0)),
	);
	
	return $price_info_arr;
}

function getbalance($tenantid){
	$multitel_username = get_multitel_setting($tenantid, 'MULTITELUSER');
	$multitel_password = get_multitel_setting($tenantid, 'MULTITELPASSWORD');
	$multitel_api_url = get_multitel_api_url();
	
	$version_api = get_version_api($tenantid);
	//$url = $multitel_api_url.'v3/getbalance';
	$url = $multitel_api_url.$version_api.'/getbalance';
	$curlopt = array(
		CURLOPT_RETURNTRANSFER => true,   // return web page
		CURLOPT_HEADER         => false,  // don't return headers
		CURLOPT_FOLLOWLOCATION => true,   // follow redirects
		CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
		CURLOPT_ENCODING       => "",     // handle compressed
		CURLOPT_USERAGENT      => "MirtaPBX", // name of client
		CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
		CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
		CURLOPT_TIMEOUT        => 120,    // time-out on response
		CURLOPT_POST           => true,
		CURLOPT_HTTPAUTH       => CURLAUTH_BASIC,
		CURLOPT_USERPWD        => "$multitel_username:$multitel_password", 
	);
	
	$ch = curl_init($url);
	curl_setopt_array($ch, $curlopt);
	$content  = curl_exec($ch);
	curl_close($ch);
	$getbalance_result_obj = json_decode($content);
	$userBalance = (!empty($getbalance_result_obj->response->balance) ? $getbalance_result_obj->response->balance : 0);
	
	return $userBalance;
}

function get_multitel_api_url(){
	$multitel_api_url = "http://api.multitel.net/";
	return $multitel_api_url;
}

function get_version_api($tenantid){
	$multitel_username = get_multitel_setting($tenantid, 'MULTITELUSER');
	$multitel_password = get_multitel_setting($tenantid, 'MULTITELPASSWORD');
	$multitel_api_url = get_multitel_api_url();
	
	$url = $multitel_api_url.'version';
	$curlopt = array(
		CURLOPT_RETURNTRANSFER => true,   // return web page
		CURLOPT_HEADER         => false,  // don't return headers
		CURLOPT_FOLLOWLOCATION => true,   // follow redirects
		CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
		CURLOPT_ENCODING       => "",     // handle compressed
		CURLOPT_USERAGENT      => "MirtaPBX", // name of client
		CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
		CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
		CURLOPT_TIMEOUT        => 120,    // time-out on response
		CURLOPT_POST           => true,
		CURLOPT_HTTPAUTH       => CURLAUTH_BASIC,
		CURLOPT_USERPWD        => "$multitel_username:$multitel_password", 
	);
	
	$ch = curl_init($url);
	curl_setopt_array($ch, $curlopt);
	$content  = curl_exec($ch);
	curl_close($ch);
	$version_result_obj = json_decode($content);
	
	$version = (!empty($version_result_obj->response->version) ? $version_result_obj->response->version : 'v3');
	return $version;
}

function sendEmailDefault($msg,$subject){
	$to = "";
	/*
	$subject = 'New number purchase';
	$msg = "Tenant ".$tenantid." has purchased phone number: ".$number;
	*/
	$headers = "";
	$headers .= "Reply-To:mail";
	$headers .= "X-Mailer: PHP/".PHPversion();
	if(mail($to, $subject, $msg, $headers)){
		//echo  __FILE__.'<br />Line : '.__LINE__.' <br /> var: <pre>'.print_r('Success', true).'</pre>';exit;
		return true;
	} else {
		//echo  __FILE__.'<br />Line : '.__LINE__.' <br /> var: <pre>'.print_r('Failed', true).'</pre>';exit;
		return false;
	}
}
?>
