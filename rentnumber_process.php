<?php
/*
 * Copyright 2018-2019 MultiTEL LLC
 * @ Website    : https://www.multitel.net
 * @ Released	: 2019/04/27

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

 */

require_once 'include/general.inc.php';
check_auth();
require_once 'include/db.inc.php';
require_once 'include/multitel.config.php';

$custom_validation_error = false;
$status = "error";
$message = "No numbers purchased";

$tenantid = ($_SESSION['selectedTenant']);

$share_tenantid = 0;
$multitel_username = get_multitel_setting($share_tenantid, 'MULTITELUSER');
$multitel_password = get_multitel_setting($share_tenantid, 'MULTITELPASSWORD');

$userBalance = getbalance($share_tenantid);

$number = (!empty($_POST['number']) ? mysql_real_escape_string(trim($_POST['number'])) : '');
$price_uuid = (!empty($_POST['price_uuid']) ? mysql_real_escape_string(trim($_POST['price_uuid'])) : '');

$getpriceinfo_obj = getpriceinfo($share_tenantid, $price_uuid);

$monthly_price = (!empty($getpriceinfo_obj->monthly_price) ? $getpriceinfo_obj->monthly_price : 0);
$setup_price = (!empty($getpriceinfo_obj->setup_price) ? $getpriceinfo_obj->setup_price : 0);
$per_minute = (!empty($getpriceinfo_obj->per_minute) ? $getpriceinfo_obj->per_minute : 0);
$give_channels = (!empty($getpriceinfo_obj->channels) ? $getpriceinfo_obj->channels : 0);

//Modified Apr 22, 2019
$per_minute = getPerMinutePrice($per_minute);
$setup_price = getSetupPrice($setup_price);
$monthly_price = getMonthlyPrice($monthly_price);

$price_per_item = $setup_price + $monthly_price;
$total_deduct = $_POST['quantity'] * $price_per_item ;

if($total_deduct > $userBalance){
	// not enough balance
	$status = 'error';
	$message = '<font color="red">Not enough credit in your account to complete purchase. You need at least $'.$total_deduct.' in your account for this purchase</font>';
}

$did_max_setup_fee = get_multitel_setting($share_tenantid, 'MULTITELDIDMAXSETUPFEE');
if(empty($did_max_setup_fee)){
	$did_max_setup_fee = 0;
}
$did_max_monthly_fee = get_multitel_setting($share_tenantid, 'MULTITELDIDMAXMONTHLYFEE');
if(empty($did_max_monthly_fee)){
	$did_max_monthly_fee = 0;
}
$did_max_minute_fee = get_multitel_setting($share_tenantid, 'MULTITELDIDMAXMINUTEFEE');
if(empty($did_max_minute_fee)){
	$did_max_minute_fee = 0;
}

$valid_max_check = true;
if($setup_price > $did_max_setup_fee or $monthly_price > $did_max_monthly_fee or $per_minute > $did_max_minute_fee){
	$valid_max_check = false;
}

if(!$valid_max_check){
	$custom_validation_error = true;
	$status = 'error';
	$message = 'Failed, price restrictions in your account prevent this purchase';
}


if(!$custom_validation_error){
	$multitel_api_url = get_multitel_api_url();
	$version_api = get_version_api($tenantid);
	
	$url = $multitel_api_url.$version_api.'/rentnumber/'.$number.'/'.$price_uuid;
	$username = $multitel_username;
	$password = $multitel_password;
	
	$uri = (!empty($_POST['uri']) ? mysql_real_escape_string(trim($_POST['uri'])) : '');
	$uri2 = (!empty($_POST['uri2']) ? mysql_real_escape_string(trim($_POST['uri2'])) : '');
	$uri3 = (!empty($_POST['uri3']) ? mysql_real_escape_string(trim($_POST['uri3'])) : '');
	$ring_type = (!empty($_POST['ring_type']) ? mysql_real_escape_string(trim($_POST['ring_type'])) : '');
	$forward_destination = (!empty($_POST['forward_destination']) ? mysql_real_escape_string(trim($_POST['forward_destination'])) : '');
	$sms_forward_destination = (!empty($_POST['sms_forward_destination']) ? mysql_real_escape_string(trim($_POST['sms_forward_destination'])) : '');
	$sms_uri = (!empty($_POST['sms_uri']) ? mysql_real_escape_string(trim($_POST['sms_uri'])) : '');
	$autorenew = (!empty($_POST['autorenew']) ? mysql_real_escape_string(trim($_POST['autorenew'])) : '');
	$overflow = (!empty($_POST['overflow']) ? mysql_real_escape_string(trim($_POST['overflow'])) : '');
	
	$params = array(
		'description' => $description,
		'uri' => $uri,
		'uri2' => $uri2,
		'uri3' => $uri3,
		'ringtype' => $forward_destination,
		'smsuri' => $sms_uri,
		'smsringtype' => $sms_forward_destination,
		'autorenew' => $autorenew,
		'overflow' => $overflow,
	);
	
	$curlopt = array(
		CURLOPT_RETURNTRANSFER => true,   // return web page
		CURLOPT_HEADER         => false,  // don't return headers
		CURLOPT_FOLLOWLOCATION => true,   // follow redirects
		CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
		CURLOPT_ENCODING       => "",     // handle compressed
		CURLOPT_USERAGENT      => "international", // name of client
		CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
		CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
		CURLOPT_TIMEOUT        => 120,    // time-out on response
		CURLOPT_POST           => true,
		CURLOPT_POSTFIELDS     => $params,
		CURLOPT_HTTPAUTH       => CURLAUTH_BASIC,
		CURLOPT_USERPWD        => "$multitel_username:$multitel_password", 
	);
	$ch = curl_init($url);
	curl_setopt_array($ch, $curlopt);
	$content  = curl_exec($ch);
	curl_close($ch);
	$rentnumber_result_obj = json_decode($content);
	//echo  __FILE__.'<br />Line : '.__LINE__.' <br /> var: <pre>'.print_r($rentnumber_result_obj, true).'</pre>';exit;
	if(!empty($rentnumber_result_obj->status->code) && $rentnumber_result_obj->status->code == 200){
		$di_dids_insert_sql = "
			INSERT INTO di_dids (di_te_id, di_number, di_admincomment, di_notifymaxchannels, di_maxchannels)
			VALUES ('".$tenantid."', '".$number."', '".$number."', 0, '".max(20, 0)."');
		";
		mysql_query($di_dids_insert_sql);
		
		$status = 'success';
		$message = 'Number rental order completed';
	} else {
		$status = 'error';
		$message = 'There was an error processing your order. Please contact support';
	}
}
$message_arr = array(
	'status' => $status,
	'message' => $message,
	'number' => $number,
);
echo json_encode($message_arr);
