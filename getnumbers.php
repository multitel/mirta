<?php
/*
 * Copyright 2018-2019 MultiTEL LLC
 * @ Website    : https://www.multitel.net
 * @ Released   : 2019/04/27

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
require_once 'include/general.inc.php';
check_auth();

require_once 'include/multitel.config.php';

$uuid = (!empty($_GET['uuid']) ? $_GET['uuid'] : '');
$uuid = addslashes($uuid);

$tenantid = 0;
$multitel_api_url = get_multitel_api_url();
$multitel_username = get_multitel_setting($tenantid, 'MULTITELUSER');
$multitel_password = get_multitel_setting($tenantid, 'MULTITELPASSWORD');
$version_api = get_version_api($tenantid);

$url = $multitel_api_url.$version_api.'/getnumbers/'.$uuid;
$curlopt = array(
	CURLOPT_RETURNTRANSFER => true,   // return web page
	CURLOPT_HEADER         => false,  // don't return headers
	CURLOPT_FOLLOWLOCATION => true,   // follow redirects
	CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
	CURLOPT_ENCODING       => "",     // handle compressed
	CURLOPT_USERAGENT      => "MirtaPBX", // name of client
	CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
	CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
	CURLOPT_TIMEOUT        => 120,    // time-out on response
	CURLOPT_POST           => true,
	CURLOPT_HTTPAUTH       => CURLAUTH_BASIC,
	CURLOPT_USERPWD        => "$multitel_username:$multitel_password", 
);

$ch = curl_init($url);
curl_setopt_array($ch, $curlopt);
$content  = curl_exec($ch);
curl_close($ch);
$getnumbers_result_obj = json_decode($content);

header('Content-Type: text/plain');

$getnumbers_html = "
<div class='row'>
	<div class='col-md-12'>
		<div class='picknumbers_container' style='padding: 20px;'>
			<table class='table default-border-top default-border-bottom' id='dt_picknumbers_".(!empty($uuid) ? $uuid : '')."' style='width:100% !important'>
				<thead>
					<th>Number</th>
					<th>Features<a href='javascript:;' class='ml-5 info_features'><i class='fa fa-info-circle'></i></a></th>
					<th>Channels<a href='javascript:;' class='ml-5 info_channels'><i class='fa fa-info-circle'></i></a></th>
					<th>Minutes<a href='javascript:;' class='ml-5 info_minutes'><i class='fa fa-info-circle'></i></a></th>
					<th>$/min<a href='javascript:;' class='ml-5 info_perminutes'><i class='fa fa-info-circle'></i></a></th>
					<th>NRC<a href='javascript:;' class='ml-5 info_setup'><i class='fa fa-info-circle'></i></a></th>
					<th>MRC<a href='javascript:;' class='ml-5 info_monthlycharge'><i class='fa fa-info-circle'></i></a></th>
					<th>YRC<a href='javascript:;' class='ml-5 info_yearlycharge'><i class='fa fa-info-circle'></i></a></th>
					<th style='width:50px'>Action</th>
				</thead>
				<tbody>
";
echo $getnumbers_html;
if(!empty($getnumbers_result_obj->response)){
	foreach ($getnumbers_result_obj->response as $getnumbers_obj) {
		$feat = '';
		$voice = 0;
		$t38 = 0;
		$sms = 0;
		$mms = 0;
		if(!empty($getnumbers_obj->features)){
			foreach ($getnumbers_obj->features as $feature) {
				switch($feature) {
					case "voice": 
						$feat.= "<li class='fa fa-mobile mr-5' alt='Voice Enabled' title='Voice Enabled'></li>"; 
						$voice = 1;
						break;
					case "t38": 
						$feat.= "<li class='fa fa-newspaper-o mr-5' alt='T.38 Enabled' title='T.38 Enabled'></li>";
						$t38 = 1;
						break;
					case "sms": 
						$feat.= "<li class='fa fa-envelope-o mr-5' alt='SMS enabled' title='SMS Enabled'></li>";
						$sms = 1;
						break;
					case "mms": 
						$feat.= "<li class='fa fa-file-video-o mr-5' alt='MMS Enabled' title='MMS Enabled'></li>";
						$mms = 1;
						break;
				}
			}
		}
		$number = (!empty($getnumbers_obj->number) ? $getnumbers_obj->number : '');
		$ss7id = (!empty($getnumbers_obj->ss7id) ? $getnumbers_obj->ss7id : '');
		$prefix_uuid = (!empty($getnumbers_obj->info->prefix_uuid) ? $getnumbers_obj->info->prefix_uuid : '');
		
		$price_uuid = '';
		$per_minute = 0;
		$setup_price = 0;
		$monthly_price = 0;
		//Modified Mar 18, 2019
		$yearly_price = 0;
		$i = 0;
		
		$option_html = "";
		if(!empty($getnumbers_obj->price)){
			foreach ($getnumbers_obj->price as $getnumbers_price_obj) {
				$getnumbers_price_channels = (!empty($getnumbers_price_obj->channels) ? $getnumbers_price_obj->channels : 0);
				$option = array(
					'uuid' => (!empty($getnumbers_price_obj->uuid) ? $getnumbers_price_obj->uuid : ''),
					'channels' => $getnumbers_price_channels,
					'setup_price' => (!empty($getnumbers_price_obj->setup_price) ? $getnumbers_price_obj->setup_price : ''),
					'monthly_price' => (!empty($getnumbers_price_obj->monthly_price) ? $getnumbers_price_obj->monthly_price : ''),
					'free_minutes' => (!empty($getnumbers_price_obj->free_minutes) ? $getnumbers_price_obj->free_minutes : ''),
					'per_minute' => (!empty($getnumbers_price_obj->per_minute) ? $getnumbers_price_obj->per_minute : ''),
					'number' => $number,
					'prefix_uuid' => $prefix_uuid,
					//Modified Mar 18, 2019
					'yearly_price' => (!empty($getnumbers_price_obj->yearly_price) ? $getnumbers_price_obj->yearly_price : ''),
				);
				$opt = json_encode($option);
				$option_selected = '';
				if ($i==0) {
					$option_selected = 'selected';
					
					$price_uuid = (!empty($getnumbers_price_obj->uuid) ? $getnumbers_price_obj->uuid : '');
					$per_minute = (!empty($getnumbers_price_obj->per_minute) ? $getnumbers_price_obj->per_minute : '0');
					$setup_price = (!empty($getnumbers_price_obj->setup_price) ? $getnumbers_price_obj->setup_price : '0');
					$monthly_price = (!empty($getnumbers_price_obj->monthly_price) ? $getnumbers_price_obj->monthly_price : '0');
					$free_minutes = (!empty($getnumbers_price_obj->free_minutes) ? $getnumbers_price_obj->free_minutes : '0');
					$yearly_price = (!empty($getnumbers_price_obj->yearly_price) ? $getnumbers_price_obj->yearly_price : 0);
					
					//Modified Apr 22, 2019
					$per_minute = getPerMinutePrice($per_minute);
					$setup_price = getSetupPrice($setup_price);
					$monthly_price = getMonthlyPrice($monthly_price);
					$yearly_price = getYearlyPrice($yearly_price);
				}
				$i++;
				$option_html .= "
					<option rel='".$price_uuid."' ".$option_selected." value='".$opt."'>
						".$getnumbers_price_channels." Dedicated
					</option>
				";
			}
		}
		$restrictions = (!empty($getnumbers_obj->restrictions) ? json_encode($getnumbers_obj->restrictions) : '');
		$features_json = (!empty($getnumbers_obj->features) ? json_encode($getnumbers_obj->features) : '');
		
		$show_buylocal_html = 'show_buylocal("'.$number.'", "'.$prefix_uuid.'", "'.$price_uuid.'");';
		$getnumbers_tr_html = "
			<tr id='picknumber_".$number."'>
				<td>
					".$number."
				</td>
				<td>
					<ul class='list-unstyled'>
					".$feat."
					</ul>
				</td>
				<td>
					<select class='channels form-control' name='channels_".$prefix_uuid."' id='".$prefix_uuid."' onchange='javascript:channelchange(this);'>
						".$option_html."
					</select>
				</td>
			<td>
				<span id='free_minutes_".$number."'>
					".$free_minutes."
				</span>
			</td>
			<td>
				<span id='per_minute_".$number."'>
					$".$per_minute."
				</span>
			</td>
			<td>
				<span id='setup_price_".$number."'>
					$".$setup_price."
				</span>
			</td>
			<td>
				<span id='monthly_price_".$number."'>
					$".$monthly_price."
				</span>
			</td>
			<td>
				<span id='yearly_price_".$number."'>
					$".$yearly_price."
				</span>
			</td>
			
			<td>
				<ul class='list-unstyled width_auto'>
					<li>
						<a href='javascript:;' class='menu_purchase btn btn-default btn-xs' alt='Purchase' title='Purchase' onclick='".$show_buylocal_html."' id='show_buylocal_".$number."'>
							<font style='font-size: 10px;'>Rent</font>
						</a>
					</li>
				</ul>
				<input type='hidden' id='restrictions_".$number."' value='$restrictions'>
				<input type='hidden' id='features_".$number."' value='$features_json'>
			</td>
		</tr>
		";
		echo $getnumbers_tr_html;
	}
}
$getnumbers_html = "
				</tbody>
			</table>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('#dt_picknumbers_".(!empty($uuid) ? $uuid : '')."').dataTable({
			'processing': true,
			'serverSide': false,
			'pagingType': 'full_numbers',
			'scrollCollapse': true,
			'info': false
		});
	});
</script>
";
echo $getnumbers_html;
?>
