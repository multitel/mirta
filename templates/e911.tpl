{include file="top.tpl" active_menu='dids'}
<link rel="stylesheet" type="text/css" media="screen" href="themes/metronic/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="themes/metronic/css/custom.css">
<div id="main" role="main">
	<div id="ribbon">
		<ol class="breadcrumb">
			<li><t>Configuration</t></li>
			<li><t>E911</t></li>
		</ol>
	</div>

	<div id="content">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="page-title txt-color-blueDark"><t>{$pagetitle}</t> - {$tenantname}</h1>
			</div>
		</div>
		<div class="panel panel-flat padding-20" id="e911-container">
			
			<div class="row">
				<div class="col-md-12">
					<form method="POST" action="" id="formE911">
						<fieldset>
							<legend>E911</legend>
							<div id="notificationContainerE911"></div>
							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<label class="control-label">
											Numbers
										</label>
										<div class="controls">
											<select class="form-control chosen input-xs" id="tn" name="tn">
												<option value="" selected>Please choose a number</option>
												{if !empty($di_dids_arr)} 
												{foreach $di_dids_arr as $di_dids_row}
													<option value="{$di_dids_row['di_number']}">
														{$di_dids_row['di_number']}
													</option>
												{/foreach}
												{/if}
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">
											Subscriber name
										</label>
										<div class="controls mt-10">
											<input type="text" name="name" class="form-control" value="" />
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label class="control-label">
											Street Number
										</label>
										<div class="controls mt-10">
											<input type="text" name="streetNum" class="form-control" value="" />
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">
											Street Name
										</label>
										<div class="controls mt-10">
											<input type="text" name="streetInfo" class="form-control" value="" />
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">
											Location
										</label>
										<div class="controls mt-10">
											<input type="text" name="location" class="form-control" value="" maxlength="20"/>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">
											City
										</label>
										<div class="controls mt-10">
											<input type="text" name="city" class="form-control" value="" />
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">
											State
										</label>
										<div class="controls">
											<select class="form-control chosen input-xs" id="state" name="state">
												<option value="" selected>Please choose a state</option>
												{if !empty($getareas_arr)} 
												{foreach $getareas_arr as $getareas_obj}
													<option value="{$getareas_obj->state_iso}">
														{$getareas_obj->name}
													</option>
												{/foreach}
												{/if}
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label class="control-label">
											ZIP Code
										</label>
										<div class="controls mt-10">
											<input type="text" name="postalCode" class="form-control" value="" />
										</div>
									</div>
								</div>
							</div>
						</fieldset>
					
						<div class="row">
							<div class="col-md-12">
								<div class="modal-footer pl-0 pr-0">
									<button type="submit" id="btnSubmitE911" class="btn btn-primary">Submit</button>
									<span class="" id="preloaderE911" style="display:none; position: absolute;right: 100px;top: 28px;">
										<img class="img-responsive" src="themes/metronic/img/ajax-loader.gif" style="height: 20px;"/>
									</span> 
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
{include file="bottom.tpl"}
<script type="text/javascript" src="//multitel.net/assets/js/plugins/forms/jquery.form.js"></script>
<script>
$(document).ready(function() {
	var E911Options = { 
		beforeSubmit:  showMainRequestE911, 
		dataType:  'json',
			success:    function(data) {  
				$('#preloaderE911').hide();
				$('#notificationContainerE911').empty();
				var notification = '<div class="alert alert-'+ data.status +'"><button data-dismiss="alert" class="close" type="button">X</button>'+data.message+'</div>';
				if(data.status == 'success'){
					$('#formE911')[0].reset();
				}
				$('#notificationContainerE911').html(notification);
				$("#btnSubmitE911").prop('disabled', false);
			},
			error: function(jqXHR) {
				console.log(jqXHR.responseText);
			}
	}; 

	$('#formE911').ajaxForm(E911Options);
} );
function showMainRequestE911(formData, jqForm, options) { 
	$('#preloaderE911').show();
	$("#btnSubmitE911").prop('disabled', true);
	return true;
}
</script>




</body>
</html>
