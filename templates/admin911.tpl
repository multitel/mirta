{include file="top.tpl" active_menu='providers'}
<link rel="stylesheet" type="text/css" media="screen" href="themes/metronic/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="themes/metronic/css/custom.css">
<div id="main" role="main">
	<div id="ribbon">
		<ol class="breadcrumb">
			<li><t>Configuration</t></li>
			<li><t>Admin E911</t></li>
		</ol>
	</div>

	<div id="content">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="page-title txt-color-blueDark"><t>{$pagetitle}</t></h1>
			</div>
		</div>
		<div class="row" style="margin-bottom:20px">
			<div class="col-md-4">
				<div class="content-group-sm">
					<label class="text-semibold">
						Tenant
					</label>
					<div class="controls">
						<select class="form-control chosen input-xs" placeholder="Tenant" id="tenant_adminnumbers" name="tenant_adminnumbers">
							<option value="0" selected>Please choose a tenant</option>
							{if !empty($te_tenants_arr)} 
							{foreach $te_tenants_arr as $te_tenants_obj}
								<option value="{$te_tenants_obj->te_id}">
									{$te_tenants_obj->te_name} {if !empty($te_tenants_obj->te_code)}- {$te_tenants_obj->te_code}{/if}
								</option>
							{/foreach}
							{/if}
						</select>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="content-group-sm">
					<label class="text-semibold">
						Show numbers
					</label>
					<div class="controls">
						<select class="form-control chosen input-xs" placeholder="Show numbers" id="show_numbers" name="show_numbers">
							<option value="local">On this pbx only</option>
							<option value="all">All my numbers</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-flat" id="results" style="padding: 20px;">
			<div class="row">
				<div class="col-xs-12">
					<h1 class="page-title txt-color-blueDark">
						<span>Admin E911</span>
					</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="">
						<table class="table table-bordered table-hover" id="dt_adminnumbers" style="width:100% !important">
							<thead>
								<tr>
									<th style="width:100px">Tenant</th>
									<th style="width:50px">Number</th>
									<th style="width:50px">Subscriber Name</th>
									<th style="width:150px">Address</th>
									<th style="width:100px">State</th>
									<th style="width:50px">City</th>
									<th style="width:50px">Zip</th>
									<th style="width:50px">Action</th>
								</tr>
							</thead>
							<tbody id="dt_adminnumbers_content">
							</tbody>
						</table>
					</div>
					<div id="modalReleaseNumber" class="modal fade" data-backdrop="false">
						<div class="modal-dialog">
							<div class="modal-content">
								<form method="POST" action="" id="formReleaseNumber">
									<div class="modal-header">
										<div class="modal_header_domain">
											<div class="row">
												<div class="col-md-11">
													<div class="ui-dialog-title custom-dialog-title"> 
														Releasing number <span id="number-tag"></span>
													</div>
												</div>
												<div class="col-md-1">
													<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span> <span class="sr-only">close</span></button>
												</div>
											</div>
										</div>
									</div>
									<div id="modalBodyReleaseNumber" class="modal-body">
										<div id="notificationContainerReleaseNumber"></div>
										<div class="modal_body_content">
											<div class="form-group">
												<label class="control-label">
													Are you sure you wish to release this number?
												</label>
												<div class="controls">
													<input type="hidden" name="number" id="number" value="">
													<select name="confirm" id="release_number_confirm" class="form-control">
														<option value="yes">Yes</option>
														<option value="no">No</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
										<button type="submit" id="btnSubmitReleaseNumber" class="btn btn-primary" disabled="disabled">Release</button>
										<span class="icon-spinner9 spinner" id="preloaderReleaseNumber" style="display:none; position: absolute;right: 185px;bottom: 25px;">
											<img class='img-responsive' src='themes/metronic/img/ajax-loader.gif' style='height: 20px;'/>
										</span> 
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
{include file="bottom.tpl"}
<script type="text/javascript" src="//multitel.net/assets/js/plugins/forms/jquery.form.js"></script>
<script>
$(document).ready(function() {
	$("body").on("change", "#tenant_adminnumbers", function(){
		generateAdminNumber();
	});
	$("body").on("change", "#show_numbers", function(){
		generateAdminNumber();
	});
	
	generateAdminNumber();
	
	/*Releasing number Start*/
	$("body").on("click", ".release_number", function(e){
		e.preventDefault();
		var number = $(this).data('number');
		$("#number").val(number);
		$("#number-tag").html(number);
		var urlProcess = "/pbx/release_911_process.php";
		$("#modalReleaseNumber").find("#formReleaseNumber").attr('action', urlProcess);
		$('#preloaderReleaseNumber').hide();
		$('#notificationContainerReleaseNumber').empty();
		$("#release_number_confirm").val('yes');
		$("#release_number_confirm").trigger('change');
		$('#modalReleaseNumber').modal();
	});
	var ReleaseNumberOptions = { 
		beforeSubmit:  showMainRequestReleaseNumber, 
		dataType:  'json',
		success:    function(data) {  
			$('#preloaderReleaseNumber').hide();
			$('#notificationContainerReleaseNumber').empty();
			var notification = '<div class="alert alert-'+ data.status +'"><button data-dismiss="alert" class="close" type="button">X</button>'+data.message+'</div>';
			if(data.status == 'success'){
				// alert("succes"); 
				$('#modalReleaseNumber').modal('hide');
				var number = $("#number").val();
				if($("#tr_adminnumber_"+number).length > 0){
					$("#tr_adminnumber_"+number).remove();
				}
				
			} else {
				$('#notificationContainerReleaseNumber').html(notification);
			}
			$("#btnSubmitReleaseNumber").prop('disabled', false);
		},
		error: function(jqXHR) {
			//var returnData = $.parseJSON(jqXHR.responseText);
			console.log(jqXHR.responseText);
			$('#modalReleaseNumber').modal('hide');
			if (jqXHR.responseText.toLowerCase().indexOf('"status":"success"') >= 0){
				var number = $("#number").val();
				if($("#tr_adminnumber_"+number).length > 0){
					$("#tr_adminnumber_"+number).remove();
				}
			}
		}
	}; 

	$('#formReleaseNumber').ajaxForm(ReleaseNumberOptions);
	$("body").on("change", "#release_number_confirm", function(e){
		e.preventDefault();
		var release_number_confirm = $(this).val();
		if(release_number_confirm == 'yes'){
			$("#btnSubmitReleaseNumber").prop('disabled', false);
		} else {
			$("#btnSubmitReleaseNumber").prop('disabled', true);
		}
	});
	/*Releasing number End*/
} );
function reloadadminNumberDatatable(){
	var dt_adminnumbers = $('#dt_adminnumbers').dataTable({
		'processing': true,
		'serverSide': false,
		'pagingType': "full_numbers",
		'scrollCollapse': true,
		'info': false
	});
}

function generateAdminNumber(){
	$('#dt_adminnumbers').DataTable().destroy();
	$('#dt_adminnumbers_content').empty().append("<td colspan='11'><center style='padding:10px'><img class='img-responsive' src='themes/metronic/img/ajax-loader.gif' style='height: 20px;'/></center></td>");
	var tenant_adminnumbers = $("#tenant_adminnumbers option:selected").val();
	var show_numbers = $("#show_numbers option:selected").val();
	$.ajax({
		type: "GET",
		url: '/pbx/getadmin911.php?te_id='+tenant_adminnumbers+'&sn='+show_numbers,
		success: function(response) {
			$("#dt_adminnumbers_content").empty().append(response);
			reloadadminNumberDatatable();
		},
		error: function(response) {
			console.log(response);
		}
	});
}
function showMainRequestReleaseNumber(formData, jqForm, options) { 
	$('#preloaderReleaseNumber').show();
	$("#btnSubmitReleaseNumber").prop('disabled', true);
	return true;
}
</script>
</body>
</html>
