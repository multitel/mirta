{include file="top.tpl" active_menu='settings'}
<link rel="stylesheet" type="text/css" media="screen" href="themes/smartadmin/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="themes/smartadmin/css/custom.css">
<div id="main" role="main">
	<div id="ribbon">
		<ol class="breadcrumb">
			<li><t>Configuration</t></li>
			<li><t>Numbers</t></li>
		</ol>
	</div>

	<div id="content">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="page-title txt-color-blueDark"><t>{$pagetitle}</t> - {$tenantname}</h1>
			</div>
		</div>
		<div class="panel panel-flat padding-20" id="e911-container">
			
			<div class="row">
				<div class="col-md-12">
					<form method="POST" action="" id="formMultitelSetting">
						<fieldset>
							<legend>Multitel Settings</legend>
							<div id="notificationContainerMultitelSetting"></div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">
											Multitel Username
										</label>
										<div class="controls">
											<input type="text" name="MULTITELUSER" class="form-control" value="{$MULTITELUSER}" />
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">
											Multitel Password
										</label>
										<div class="controls">
											<input type="text" name="MULTITELPASSWORD" class="form-control" value="{$MULTITELPASSWORD}" />
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">
											Server IP Address
										</label>
										<div class="controls">
											<input type="text" name="SERVERIPADDRESS" class="form-control" value="{$SERVERIPADDRESS}" />
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<label class="control-label">
											Multitel Max Setup Fee
										</label>
										<div class="controls">
											<input type="text" name="MULTITELDIDMAXSETUPFEE" class="form-control" value="{$MULTITELDIDMAXSETUPFEE}" />
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label class="control-label">
											Multitel Max Monthly Fee
										</label>
										<div class="controls">
											<input type="text" name="MULTITELDIDMAXMONTHLYFEE" class="form-control" value="{$MULTITELDIDMAXMONTHLYFEE}" />
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label class="control-label">
											Multitel Max Minute Fee
										</label>
										<div class="controls">
											<input type="text" name="MULTITELDIDMAXMINUTEFEE" class="form-control" value="{$MULTITELDIDMAXMINUTEFEE}" />
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label class="control-label">
											Setup Markup
										</label>
										<div class="controls">
											<input type="text" name="MULTITELSETUPMARKUP" class="form-control" value="{$MULTITELSETUPMARKUP}" />
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<label class="control-label">
											Monthly Markup
										</label>
										<div class="controls">
											<input type="text" name="MULTITELMONTHLYMARKUP" class="form-control" value="{$MULTITELMONTHLYMARKUP}" />
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label class="control-label">
											Yearly Markup
										</label>
										<div class="controls">
											<input type="text" name="MULTITELYEARLYMARKUP" class="form-control" value="{$MULTITELYEARLYMARKUP}" />
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label class="control-label">
											Per Minute Markup
										</label>
										<div class="controls">
											<input type="text" name="MULTITELPERMINUTEMARKUP" class="form-control" value="{$MULTITELPERMINUTEMARKUP}" />
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label class="control-label">
											Sale Price E911
										</label>
										<div class="controls">
											<input type="text" name="MULTITELSALEPRICEE911" class="form-control" value="{$MULTITELSALEPRICEE911}" />
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">
											Email Notification Address
										</label>
										<div class="controls">
											<input type="text" name="MULTITELEMAILNOTIFICATIONADDRESS" class="form-control" value="{$MULTITELEMAILNOTIFICATIONADDRESS}" />
										</div>
									</div>
								</div>
							</div>
						</fieldset>
						<div class="row">
							<div class="col-md-12">
								<div class="modal-footer pl-0 pr-0">
									<button type="submit" id="btnSubmitMultitelSetting" class="btn btn-primary">Save Change</button>
									<input type="hidden" name="save_setting" value="1" />
									<span class="" id="preloaderMultitelSetting" style="display:none; position: absolute;right: 125px;top: 28px;">
										<img class="img-responsive" src="themes/smartadmin/img/ajax-loader.gif" style="height: 20px;"/>
									</span> 
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
{include file="bottom.tpl"}
<script type="text/javascript" src="//multitel.net/assets/js/plugins/forms/jquery.form.js"></script>
<script>
$(document).ready(function() {
	var MultitelSettingOptions = { 
		beforeSubmit:  showMainRequestMultitelSetting, 
		dataType:  'json',
			success:    function(data) {  
				$('#preloaderMultitelSetting').hide();
				$('#notificationContainerMultitelSetting').empty();
				var notification = '<div class="alert alert-'+ data.status +'"><button data-dismiss="alert" class="close" type="button">X</button>'+data.message+'</div>';
				$('#notificationContainerMultitelSetting').html(notification);
				$("#btnSubmitMultitelSetting").prop('disabled', false);
			},
			error: function(jqXHR) {
				console.log(jqXHR.responseText);
			}
	}; 

	$('#formMultitelSetting').ajaxForm(MultitelSettingOptions);
} );
function showMainRequestMultitelSetting(formData, jqForm, options) { 
	$('#preloaderMultitelSetting').show();
	$("#btnSubmitMultitelSetting").prop('disabled', true);
	return true;
}
</script>




</body>
</html>
