{include file="top.tpl" active_menu='dids'}
<link rel="stylesheet" type="text/css" media="screen" href="themes/smartadmin/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="themes/smartadmin/css/custom.css">
<div id="main" role="main">
	<div id="ribbon">
		<ol class="breadcrumb">
			<li><t>Configuration</t></li>
			<li><t>Numbers</t></li>
		</ol>
	</div>

	<div id="content">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="page-title txt-color-blueDark"><t>{$pagetitle}</t> - {$tenantname}</h1>
			</div>
		</div>
		<div class="row" style="margin-bottom:20px">
			<div class="col-md-4">
				<div class="content-group-sm">
					<label class="text-semibold">Country:</label>
					<select class="form-control chosen input-xs" placeholder="Country" id="country" name="country">
						<option value="XX" selected>Please choose a country</option>
						{if !empty($countries_arr)} 
						{foreach $countries_arr as $countries_obj}
							<option value="{$countries_obj->isocode}">
								{$countries_obj->name} {if !empty($countries_obj->countrycode)}[+ {$countries_obj->countrycode}]{/if}
							</option>
						{/foreach}
						{/if}
					</select>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="content-group-sm">
					<label class="text-semibold">Area:</label>
					<select class="form-control chosen input-xs" placeholder="Area" id="area" name="area" disabled>
						<option selected value='XX'>Please choose an area</option>
					</select>
					<div class="" id="preloaderCountry" style="display:none; position: absolute;top: 45px;right: -15px;">
						<img class="img-responsive" src="themes/smartadmin/img/loading.gif" />
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-flat hidden" id="results" style="padding: 20px;">
			<div class="row">
				<div class="col-xs-12">
					<h1 class="page-title txt-color-blueDark">
						<span id="area_selected"></span> <span>Prefixes/Area codes</span>
					</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="" id="bestvalueresults">
						<table class="table table-bordered table-hover" id="dt_greprefix" style="width:100% !important">
							<thead>
								<tr>
									<th width="5%">Prefix</th>
									<th width="5%">Name</th>
									<th width="5%">Metered<a href='javascript:;' class='ml-5 info_metered'><i class='fa fa-info-circle'></i></a></th>
									<th width="5%">Overflow<a href='javascript:;' class='ml-5 info_overflow'><i class='fa fa-info-circle'></i></a></th>
									<th width="5%">Needs Registration<a href='javascript:;' class='ml-5 info_needs_registration'><i class='fa fa-info-circle'></i></a></th>
									<th width="5%">Action</th>
								</tr>
							</thead>
							<tbody id="local">
								<td colspan="11" id="localresults">
									<center>
										<img class="img-responsive" src="themes/smartadmin/img/ajax-loader.gif" style="height: 20px;"/>
									</center>
								</td>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="modalPurchase" class="modal fade" data-backdrop="false">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form method="POST" action="" id="formPurchase">
					<div class="modal-header">
						<div class="modal_header_domain">
							<div class="row">
								<div class="col-md-11">
									<div class="ui-dialog-title custom-dialog-title"> Rent number </div>
								</div>
								<div class="col-md-1">
									<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span> <span class="sr-only">close</span></button>
								</div>
							</div>
						</div>
					</div>
					<div id="modalBodyPurchase" class="modal-body">
						<div id="notificationContainerPurchase"></div>
						<div class="modal_body_content clear_body_content"> </div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						<button type="submit" id="btnSubmitPurchase" class="btn btn-primary">Purchase</button>
						<span class="icon-spinner9 spinner" id="preloaderPurchase" style="display:none; position: absolute;right: 185px;bottom: 25px;">
							<img class='img-responsive' src='themes/smartadmin/img/ajax-loader.gif' style='height: 20px;'/>
						</span> 
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<div id="modalRegistration" class="modal fade" data-backdrop="false">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal_header_domain">
						<div class="row">
							<div class="col-md-11">
								<div class="ui-dialog-title custom-dialog-title"> Registration info </div>
							</div>
							<div class="col-md-1">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span> <span class="sr-only">close</span></button>
							</div>
						</div>
					</div>
				</div>
				<div id="modalBodyRegistration" class="modal-body">
					<div id="notificationContainerRegistration"></div>
					<div class="modal_body_content clear_body_content"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	
	<div id="modalinfoChannels" class="modal fade" data-backdrop="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal_header_domain">
						<div class="row">
							<div class="col-md-11">
								<div class="ui-dialog-title custom-dialog-title"> Channels info </div>
							</div>
							<div class="col-md-1">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span> <span class="sr-only">close</span></button>
							</div>
						</div>
					</div>
				</div>
				<div id="modalBodyinfoChannels" class="modal-body">
					<div class="modal_body_content">
						<div>
							<strong>0 Channels</strong> - your number does not have any dedicated capacity, so all inbound calls will be allowed and charged at the listed rate per minute. Usually limited to 900 simultaneous calls per number (but limit can be increased if you need the extra capacity).
						</div>
						<div>
							<strong>2 Channels</strong> - your number will have a dedicated capacity, so inbound calls will be allowed free of charge, while using the free inbound allocated minutes and while only using only two simultaneous calls.  Additional simultaneous calls are allowed but charged at the listed rate per minute
						</div>
						<div>
							<strong>More channels</strong> - your number will have this upper limit of simultaneous calls. If a quota of free inbound minutes is provided, your calls will be free of charge while within this quota. If 0 free inbound minutes are provided, your calls will be charged the per minute rate from the first call you receive.
						</div>
						<div class="pt-10">
							Note that, where present, free inbound minutes will be replenished monthly.
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="modalinfoFeatures" class="modal fade" data-backdrop="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal_header_domain">
						<div class="row">
							<div class="col-md-11">
								<div class="ui-dialog-title custom-dialog-title"> Features info </div>
							</div>
							<div class="col-md-1">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span> <span class="sr-only">close</span></button>
							</div>
						</div>
					</div>
				</div>
				<div id="modalBodyinfoFeatures" class="modal-body">
					<div class="modal_body_content">
						<div class="voip-container mt-5">
							<li class='fa fa-mobile mr-5' alt='Voice Enabled' title='Voice Enabled'></li>: 
							<span>
								This phone number can receive inbound calls
							</span>
						</div>
						<div class="SMS-container mt-5">
							<li class='fa fa-envelope-o mr-5' alt='Inbound (one way) SMS enabled' title='Inbound (one way) SMS Enabled'></li>: 
							<span>
								This phone number can receive SMS messages
							</span>
						</div>
						<div class="MMS-container mt-5">
							<li class='fa fa-file-video-o mr-5' alt='Inbound (one way) MMS Enabled' title='Inbound (one way) MMS Enabled'></li>: 
							<span>
								This phone number can receive MMS messages
							</span>
						</div>
						<div class="T38-container mt-5">
							<li class='fa fa-newspaper-o mr-5' alt='T.38 Enabled' title='T.38 Enabled'></li>: 
							<span>
								This phone number can receive fax messages
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="modalinfoMinutes" class="modal fade" data-backdrop="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal_header_domain">
						<div class="row">
							<div class="col-md-11">
								<div class="ui-dialog-title custom-dialog-title"> Minutes info </div>
							</div>
							<div class="col-md-1">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span> <span class="sr-only">close</span></button>
							</div>
						</div>
					</div>
				</div>
				<div id="modalBodyinfoMinutes" class="modal-body">
					<div class="modal_body_content">
						This is the amount (quota) of free inbound minutes associated with this number. Your calls will not get charged while the amount of calls (duration) you receive each month stays below this amount of minutes. You may only be charged per minute charges (see the $/minute column) if you go over this quota of free inbound minutes, or if the number of simultaneous calls you receive is greater than the number of dedicated channels this number is assigned and if "Overflow" is enabled on this number.  You can turn on/off the "Overflow" functionality when you purchase the number, or , later on, when you edit the number
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="modalinfoPerminutes" class="modal fade" data-backdrop="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal_header_domain">
						<div class="row">
							<div class="col-md-11">
								<div class="ui-dialog-title custom-dialog-title"> $/minute info </div>
							</div>
							<div class="col-md-1">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span> <span class="sr-only">close</span></button>
							</div>
						</div>
					</div>
				</div>
				<div id="modalBodyinfoPerminutes" class="modal-body">
					<div class="modal_body_content">
						This is what you will be charged per minute, for inbound calls.  If this number has any dedicated capacity with free inbound minutes, you will only get charged this per minute rate for calls that exceed the amount of free minutes , or if the amount of simultaneous calls is greater than the number of dedicated channels this number is assigned and if "Overflow" is enabled on this number.  You can turn on/off the "Overflow" functionality when you purchase the number, or , later on, when you edit the number
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="modalinfoNRC" class="modal fade" data-backdrop="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal_header_domain">
						<div class="row">
							<div class="col-md-11">
								<div class="ui-dialog-title custom-dialog-title"> NRC info </div>
							</div>
							<div class="col-md-1">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span> <span class="sr-only">close</span></button>
							</div>
						</div>
					</div>
				</div>
				<div id="modalBodyinfoNRC" class="modal-body">
					<div class="modal_body_content">
						This is a Non Recurrent Charge , also known as a Setup fee. This is a one time cost that will be charged at the time you are renting this phone number. We are getting charged NRC/setup fees by our vendors as well and we choose to pass on these fees further to you, so that we can keep your monthly recurring charges lower
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="modalinfoMRC" class="modal fade" data-backdrop="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal_header_domain">
						<div class="row">
							<div class="col-md-11">
								<div class="ui-dialog-title custom-dialog-title"> MRC info </div>
							</div>
							<div class="col-md-1">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span> <span class="sr-only">close</span></button>
							</div>
						</div>
					</div>
				</div>
				<div id="modalBodyinfoMRC" class="modal-body">
					<div class="modal_body_content">
						This is a Monthly Recurrent Charge. This is what you will get charged as a fixed fee, each month, for this phone number. You can cancel this number any time as these numbers don't have a minimum contract duration.
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="modalinfoYRC" class="modal fade" data-backdrop="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal_header_domain">
						<div class="row">
							<div class="col-md-11">
								<div class="ui-dialog-title custom-dialog-title"> YRC info </div>
							</div>
							<div class="col-md-1">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span> <span class="sr-only">close</span></button>
							</div>
						</div>
					</div>
				</div>
				<div id="modalBodyinfoYRC" class="modal-body">
					<div class="modal_body_content">
						This is the Yearly Recurrent Charge. This is what you will get charged as a fixed fee yearly, for this phone number. Due to the nature of our contracts with our vendors, these numbers have a year to year contract duration.  If this duration is too long, we might have alternatives that have a month to month minimum contract duration.
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="modalinfoMetered" class="modal fade" data-backdrop="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal_header_domain">
						<div class="row">
							<div class="col-md-11">
								<div class="ui-dialog-title custom-dialog-title"> Metered info </div>
							</div>
							<div class="col-md-1">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span> <span class="sr-only">close</span></button>
							</div>
						</div>
					</div>
				</div>
				<div id="modalBodyinfoMetered" class="modal-body">
					<div class="modal_body_content">
						These numbers do not include free inbound minutes and will be charged a per minute fee from the first received inbound call
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="modalinfoOverflow" class="modal fade" data-backdrop="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal_header_domain">
						<div class="row">
							<div class="col-md-11">
								<div class="ui-dialog-title custom-dialog-title"> Overflow info </div>
							</div>
							<div class="col-md-1">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span> <span class="sr-only">close</span></button>
							</div>
						</div>
					</div>
				</div>
				<div id="modalBodyinfoOverflow" class="modal-body">
					<div class="modal_body_content">
						If Overflow is allowed, and if you choose to enable it for your number, it will allow you to receive more simultaneous calls than the dedicated channel capacity for your number allows it. You will be charged per minute fees for these additional calls. Look at the $/minute information to see the cost per minute you will be charged.
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="modalinfoNeedsRegistration" class="modal fade" data-backdrop="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal_header_domain">
						<div class="row">
							<div class="col-md-11">
								<div class="ui-dialog-title custom-dialog-title"> Needs Registration info </div>
							</div>
							<div class="col-md-1">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span> <span class="sr-only">close</span></button>
							</div>
						</div>
					</div>
				</div>
				<div id="modalBodyinfoNeedsRegistration" class="modal-body">
					<div class="modal_body_content">
						Local rules and regulations for this country require us to collect end-user information, related documents and usage information. Numbers will only be activated once this information is approved and verified to be true and accurate by local requesting authorities. You should only go ahead with the purchase if you are able to provide the required information/documents, as the cost of your purchase is not refundable if you fail to provide these documents.
					</div>
				</div>
			</div>
		</div>
	</div>
{include file="bottom.tpl"}
<script type="text/javascript" src="//multitel.net/assets/js/plugins/forms/jquery.form.js"></script>
<script>
$(document).ready(function() {
	$('#country').on("change",function() {
		var country = $("#country option:selected").val();
		$('#local').empty().append("<td colspan='11' id='localresults'><center style='padding:10px'><img class='img-responsive' src='themes/smartadmin/img/ajax-loader.gif' style='height: 20px;'/></center>");
		$('#results').addClass('hidden');
		$("#preloaderCountry").show();
		if (country!='XX') { 
			$("#area").prop("disabled", true).trigger("chosen:updated");
			var country_selected = $("#country option:selected").val();
			$.ajax({
				type: "POST",
				url: '/pbx/getareas.php',
				data: { isocode:  country_selected}, 
				success: function(response) {
					$("#area").empty().append(response);
					$("#area").prop("disabled", false).trigger("chosen:updated");
					$("#preloaderCountry").hide();
				},
				error: function() {
					$("#area").prop("disabled", true).trigger("chosen:updated");
					$("#preloaderCountry").hide();
				}
			});
		} else { 
			$("#area").prop("disabled", true).trigger("chosen:updated");
			$("#preloaderCountry").hide();
		}
	});
	
	$('#area').on("change",function() {
		var area_name = $('#area option:selected').text();
		$("#area_selected").text(area_name);
		$('#results').removeClass('hidden');
		if ($('#area option:selected').val()!='XX') {
			$('#dt_greprefix').DataTable().destroy();
			
			var choice = $.parseJSON($('#area option:selected').val());
			$('#local').empty().append("<td colspan='11' id='localresults'><center style='padding:10px'><img class='img-responsive' src='themes/smartadmin/img/ajax-loader.gif' style='height: 20px;'/></center></td>");
			$.ajax({
				type: "GET",
				url: '/pbx/getprefixes.php?uuid='+ choice.uuid,
				success: function(response) {
					$('#bestvalueresults').removeClass('hidden');
					$("#local").empty().append(response);
					reloadprefixDatatable();
				},
				error: function(response) {
					console.log(response);
				}
			});
		}
	});
	
	/*Pick Number Start*/
	$('#dt_greprefix').on('click', '.show_picknumbers', function () {
		var tr = $(this).closest('tr');
		var row = $('#dt_greprefix').DataTable().row( tr );
		
		var uuid = $(this).data('uuid');
		if ( row.child.isShown() ) {
			// This row is already open - close it
			row.child.hide();
			tr.removeClass('shown');
		} else {
			//Hide each row shown
			$('#dt_greprefix').DataTable().rows().eq(0).each( function ( idx ) {
				var rowHide = $('#dt_greprefix').DataTable().row( idx );
				if ( rowHide.child.isShown() ) {
					rowHide.child.hide();
				}
			} );
			
			// Open this row
			var url = "/pbx/getnumbers.php?uuid="+uuid;
			var loader_html = "<center style='padding:10px'><img class='img-responsive' src='themes/smartadmin/img/ajax-loader.gif' style='height: 20px;'/></center>";
			row.child(loader_html).show();
			$.ajax({
				url: url,
			}).done(function(content) {
				row.child(content, 'subtable_prefix').show();
			});
			tr.addClass('shown');
		}
	});
	
	/*Pick Number End*/
	/*Icon Info Start*/
	$("body").on("click", ".info_channels", function(e){
		e.preventDefault();
		$("#modalinfoChannels").modal();
	});
	$("body").on("click", ".info_features", function(e){
		e.preventDefault();
		$("#modalinfoFeatures").modal();
	});
	$("body").on("click", ".info_minutes", function(e){
		e.preventDefault();
		$("#modalinfoMinutes").modal();
	});
	$("body").on("click", ".info_perminutes", function(e){
		e.preventDefault();
		$("#modalinfoPerminutes").modal();
	});
	$("body").on("click", ".info_setup", function(e){
		e.preventDefault();
		$("#modalinfoNRC").modal();
	});
	$("body").on("click", ".info_monthlycharge", function(e){
		e.preventDefault();
		$("#modalinfoMRC").modal();
	});
	$("body").on("click", ".info_yearlycharge", function(e){
		e.preventDefault();
		$("#modalinfoYRC").modal();
	});
	$("body").on("click", ".info_metered", function(e){
		e.preventDefault();
		$("#modalinfoMetered").modal();
	});
	$("body").on("click", ".info_overflow", function(e){
		e.preventDefault();
		$("#modalinfoOverflow").modal();
	});
	$("body").on("click", ".info_needs_registration", function(e){
		e.preventDefault();
		$("#modalinfoNeedsRegistration").modal();
	});
	/*Icon Info End*/
	/*Purchase Start*/
	var PurchaseOptions = { 
		beforeSubmit:  showMainRequestPurchase, 
		dataType:  'json',
			success:    function(data) {  
				$('#preloaderPurchase').hide();
				$('#notificationContainerPurchase').empty();
				var notification = '<div class="alert alert-'+ data.status +'"><button data-dismiss="alert" class="close" type="button">X</button>'+data.message+'</div>';
				if(data.status == 'success'){
					// alert("succes"); 
					$('#modalPurchase').modal('hide');
					if(data.number != ''){
						if($("#picknumber_"+data.number).length > 0){
							$("#picknumber_"+data.number).remove();
						}
					}
				} else {
					$('#notificationContainerPurchase').html(notification);
				}
				$("#btnSubmitPurchase").prop('disabled', false);
			},
			error: function(jqXHR) {
				console.log(jqXHR.responseText);
			}
	}; 

	$('#formPurchase').ajaxForm(PurchaseOptions);
	/*Purchase End*/
} );
function reloadprefixDatatable(){
	var dt_greprefix = $('#dt_greprefix').dataTable({
		'processing': true,
		'serverSide': false,
		'pagingType': "full_numbers",
		'scrollCollapse': true,
		'info': false
	});
}
function show_registration(uuid) {
	$('#modalRegistration').modal();
	var contentRestriction = $("#restrictions_"+uuid).html();
	
	$('#modalBodyRegistration').find('.modal_body_content').html(contentRestriction);
}
function channelchange(e) { 
	var choice = $.parseJSON($('option:selected',e).val());
	
	var free_minutes = choice.free_minutes;
	if(free_minutes == ''){
		free_minutes = 0;
	}
	var per_minute = choice.per_minute;
	if(per_minute == ''){
		per_minute = 0;
	}
	var setup_price = choice.setup_price;
	if(setup_price == ''){
		setup_price = 0;
	}
	var monthly_price = choice.monthly_price;
	if(monthly_price == ''){
		monthly_price = 0;
	}
	$('#free_minutes_' + choice.number).empty().append(free_minutes);
	$('#per_minute_' + choice.number).empty().append('$' + per_minute);
	$('#setup_price_' + choice.number ).empty().append('$' + setup_price);
	$('#monthly_price_'+ choice.number).empty().append('$' + monthly_price);
	$("#show_buylocal_"+choice.number).attr('onclick', "show_buylocal('"+choice.number+"', '"+choice.prefix_uuid+"', '"+choice.uuid+"')")
	//$("#menu_add_to_cart_"+choice.number).data('price_uuid', choice.uuid);
}
/*Purchase Start*/
function show_buylocal(number, prefix_uuid, price_uuid){
	var url = "/pbx/rentnumber.php";
	var urlProcess = "/pbx/rentnumber_process.php";
	$("#modalPurchase").find("#formPurchase").attr('action', urlProcess);
	$("#btnSubmitPurchase").prop('disabled', false);
	$('#preloaderPurchase').hide();
	$('#notificationContainerPurchase').empty();
	$('#modalPurchase').modal();
	$(".clear_body_content").html('');
	var loader_html = "<div class='center'><img src='themes/smartadmin/img/ajax-loader.gif' class='img-responsive' style='margin:auto; height: 50px;'/></div>";
	$('#modalBodyPurchase').find('.modal_body_content').html(loader_html);
	var restrictions = '';
	if($("#restrictions_"+number).length > 0){
		restrictions = $("#restrictions_"+number).val();
	}
	var features = '';
	if($("#features_"+number).length > 0){
		features = $("#features_"+number).val();
	}
	
	$.ajax({
		url: url,
		type: "POST",
		data: { number:  number, prefix_uuid:prefix_uuid, price_uuid:price_uuid, restrictions:restrictions, features:features},
	}).done(function(content) {
		$('#modalBodyPurchase').find('.modal_body_content').html(content);
	});
}
function showMainRequestPurchase(formData, jqForm, options) { 
	$('#preloaderPurchase').show();
	$("#btnSubmitPurchase").prop('disabled', true);
	return true;
}
/*Purchase End*/
</script>

</body>
</html>
