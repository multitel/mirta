<?php
/*
 * Copyright 2018-2019 MultiTEL LLC
 * @ Website    : https://www.multitel.net
 * @ Released   : 2019/04/27

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

require_once 'include/general.inc.php';
check_auth();

if (empty($isocode)) {
	$isocode = mysql_real_escape_string($_POST['isocode']);
}
require_once 'include/multitel.config.php';

$tenantid = 0;
$multitel_api_url = get_multitel_api_url();
$multitel_username = get_multitel_setting($tenantid, 'MULTITELUSER');
$multitel_password = get_multitel_setting($tenantid, 'MULTITELPASSWORD');
$version_api = get_version_api($tenantid);

$url = $multitel_api_url.$version_api.'/getareas/'.$isocode;
$curlopt = array(
	CURLOPT_RETURNTRANSFER => true,   // return web page
	CURLOPT_HEADER         => false,  // don't return headers
	CURLOPT_FOLLOWLOCATION => true,   // follow redirects
	CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
	CURLOPT_ENCODING       => "",     // handle compressed
	CURLOPT_USERAGENT      => "MirtaPBX", // name of client
	CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
	CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
	CURLOPT_TIMEOUT        => 120,    // time-out on response
	CURLOPT_POST           => true,
	CURLOPT_HTTPAUTH       => CURLAUTH_BASIC,
	CURLOPT_USERPWD        => "$multitel_username:$multitel_password", 
);
$ch = curl_init($url);
curl_setopt_array($ch, $curlopt);
$content  = curl_exec($ch);
curl_close($ch);
$getareas_obj = json_decode($content);

$getareas_arr = (!empty($getareas_obj->response) ? $getareas_obj->response : array());

header('Content-Type: text/plain');
?>
<option value="XX" selected>Please choose an area</option>
<?php
if(!empty($getareas_arr)){
	foreach ($getareas_arr as $getareas_obj) {
		?>
		<option value='{"uuid":"<?=(!empty($getareas_obj->uuid) ? $getareas_obj->uuid : '');?>"}'><?=(!empty($getareas_obj->name) ? $getareas_obj->name : '');?></option>
		<?php
	}
}
?>
