# MultiTEL MirtaPBX module

![MultiTEL](https://multitel.net/assets/multitel.png)

Add-on for MultiTEL API services integration with MirtaPBX customers. 

Provides access to :
- E911 
- Number/DID ordering

Following API functions are used:
*E911:*

https://multitel.net/docs/view/401/restjson-services/v3-e911-inventory

https://multitel.net/docs/view/381/restjson-services/v3-e911-provision-address

https://multitel.net/docs/view/391/restjson-services/v3-e911-delete-address

*Numbers:*

https://multitel.net/docs/view/251/restjson-services/v3-putting-it-all-together

Prerequisites:
- You will need a working MirtaPBX installation
- You will need to have a MultiTEL account.
- You will need to know your server's IP address (the one you're sending calls from)

# Installation:

## Set the correct folder for your MirtaPBX installation:

- export DEST=/var/www/pbx
- echo $DEST # should show the correct folder
- cd /tmp
- git clone https://gitlab.com/multitel/mirta.git mirta.git
- cd mirta.git
- cp include/* $DEST/include/
- cp *.php $DEST/
- cp templates/* $DEST/templates

# Configuration

## Web access:

- Edit your MultiTEL credentials. 
  If you do not want to use your regular MultiTEL login, you can create a separate API user, here:
  https://www.multitel.net/settings (go to the "API Users" tab)
 
  Open up this page in your MirtaPBX installation : /pbx/multitelsettings.php

  You will be required to fill in the following fields:
  - MultiTEL Username:  Your MultiTEL API username
  - MultiTEL Password:  Your MultiTEL API password
  - Server IP address:  This is the IP address (or FQDN) where we will be sending you calls
  - MultiTEL MAX Setup fee:  Only allow your end users to purchase phone numbers if the number setup fee is below this value (example 5 for $5USD)
  - MultiTEL MAX Monthly fee: similar to the value above, but referring to Monthly fees
  - MultiTEL MAX Minute fee: similar to the value above, but referring to the per minute charges on inbound calls
  - Setup Markup: show your customers marked up prices. Sample value: 10  <- this will add a 10% markup when prices are shown to your customers.
  For example, if your customers wish to purchase a phone number that cost $1/month, they will actually see it as $1.10/month
  - Monthly Markup: similar to the above , but referring to the Monthly Fees (if any) (example: 10 for a 10% markup)
  - Yearly Markup: similar to the above , but referring to the Yearly Fees (if any) (example: 10 for a 10% markup)
  - Per Minute Markup: similar to the above, but referring to the Per Minute Fees for inbound calls (example:  15 for a 15% markup)
  - Sale price E911 - this determines what monthly price you will be charging your customers for E911 provisions. This is expressed in $ dollars /month
  Example: 1 for $1 USD / month
  - Email address: this will be the email address where we will be sending you notifications when a new number is purchased or provisioned with E911 (TODO)

  These values will be saved in MirtaPBX's database.
  
  ###TODO: 
  - Complete the emailing code
  - Don't know if there's a cron job for charging services against MirtaPBX tenants on a monthly basis.

## Inbound and outbound calls 

- Add one or more of MultiTEL's SBCs to your MirtaPBX server. This would allow you to receive inbound calls as well as send outbound calls (be it E911 or regular calls)
  This should be under /pbx/provider.php in your MirtaPBX installation
  The SIP Proxy you will be sending calls to should be one of the following:
  United States: sbc-us.multitel.net
  Canada : sbc-ca.multitel.net
  South America: sbc-br.multitel.net
  Europe:  sbc-nl.multitel.net, or sbc-de.multitel.net or sbc-uk.multitel.net
  Asia: sbc-sg.multitel.net
  Africa: sbc-za.multitel.net
  Australia: sbc-au.multitel.net

## Phone numbers:
- Set your Default SIP URI here:
  https://www.multitel.net/settings (go to the "URI Settings" tab)

## E911:
- Authorize the IP addresses you will be sending E911 calls from:
  https://www.multitel.net/trunks/ips

- Create the correct dialing rules for outbound calls towards E911. 
  This is done under /pbx/routingprofiles.php in your MirtaPBX installation. 
  Edit your routing profile for United States/Canada (or your default profile if you only have one), and add at least the following values:

  | Name | Node     | Regex | Provider | Digits to add | Digits to del | Use LCR | Order | Priority | Weight | Actions |
  | ---- | -------- | ----- | -------- | ------------- | ------------- | ------- | ----- | -------- | ------ | ------- | 
  | 711  | Any node | ^711$ | MultiTEL |               |             0 |         |     0 |        1 |      1 |         |
  | 911  | Any node | ^911$ | MultiTEL |               |             0 |         |     0 |        1 |      1 |         |     
  
  
  # How to use it:
  
  ## Admin Configuration: 
  Visit: /pbx/multitelsettings.php
  
  ## Customer view for purchasing phone numbers
  Visit: /pbx/numbers.php
  (After successful purchase, numbers are added to /pbx/dids.php)
  
  ## Admin view for purchased phone numbers
  Visit: /pbx/adminnumbers.php
  (When deleting, you have the option for deleting from MultiTEL API - and leave number in /pbx/dids.php - or from both)
  
  ## Customer view for E911 provisioning
  Visit: /pbx/e911.php
  (Prerequisite: you need to already have United States/Canadian numbers in your /pbx/dids.php section)
  Once you provision a number/address with E911 , when editing the number in /pbx/dids.php , it will show the address.
  
  ## Admin view for E911 provisioning
  Visit: /pbx/admin911.php

  ## Look and feel
  This project is using smarty (same framework MirtaPBX is using) so it integrates almost 100% with the original  software. 
  What is missing are the links on right hand side menu (since the menu file is encrypted, we cannot add these links)

  You can see some printscreens below, so at least you know how it looks like before you install it.
  
  ** Country listing **

  ![Country list](https://multitel.net/assets/mirta/mirtacountry.png)

  ** Areas listing **

  ![Area listing](https://multitel.net/assets/mirta/mirtaareas.png)

  ** Number listing **

  ![Number listing](https://multitel.net/assets/mirta/mirtanumbers.png)

  ** Rent number **
  
  ![Rent number](https://multitel.net/assets/mirta/mirtarent.png)

  ** E911 provisioning **

  ![E911 provisioning](https://multitel.net/assets/mirta/mirtae911.png)

  ## Contact
  
  Questions? Comments? Help?
  https://www.multitel.net/contact
