<?php
/*
 * Copyright 2018-2019 MultiTEL LLC
 * @ Website    : https://www.multitel.net
 * @ Released   : 2019/04/27

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

require_once 'include/general.inc.php';
check_auth();

require_once 'include/db.inc.php';
require_once 'include/multitel.config.php';

$status = '';
$message = '';
$tenantid = ($_SESSION['selectedTenant']);

if(!empty($_POST)){
	$custom_validation_error = false;
	
	$tn = (!empty($_POST['tn']) ? mysql_real_escape_string(trim($_POST['tn'])) : '');
	$name = (!empty($_POST['name']) ? mysql_real_escape_string(trim($_POST['name'])) : '');
	$streetNum = (!empty($_POST['streetNum']) ? mysql_real_escape_string(trim($_POST['streetNum'])) : '');
	$streetInfo = (!empty($_POST['streetInfo']) ? mysql_real_escape_string(trim($_POST['streetInfo'])) : '');
	$location = (!empty($_POST['location']) ? mysql_real_escape_string(trim($_POST['location'])) : '');
	$city = (!empty($_POST['city']) ? mysql_real_escape_string(trim($_POST['city'])) : '');
	$state = (!empty($_POST['state']) ? mysql_real_escape_string(trim($_POST['state'])) : '');
	$postalCode = (!empty($_POST['postalCode']) ? mysql_real_escape_string(trim($_POST['postalCode'])) : '');
	
	if(empty($tn)){
		$custom_validation_error = true;
		$status = 'error';
		$message = 'The Numbers field is required.';
	}
	if(empty($name)){
		$custom_validation_error = true;
		$status = 'error';
		$message = 'The Subscriber name field is required.';
	}
	if(empty($streetNum)){
		$custom_validation_error = true;
		$status = 'error';
		$message = 'The Street Number field is required.';
	}
	if(empty($streetInfo)){
		$custom_validation_error = true;
		$status = 'error';
		$message = 'The Street Name field is required.';
	}
	if(empty($city)){
		$custom_validation_error = true;
		$status = 'error';
		$message = 'The City field is required.';
	}
	if(empty($state)){
		$custom_validation_error = true;
		$status = 'error';
		$message = 'The State field is required.';
	}
	if(empty($postalCode)){
		$custom_validation_error = true;
		$status = 'error';
		$message = 'The ZIP Code field is required.';
	}
	
	$di_dids_sql = "SELECT * from di_dids WHERE di_te_id='".$tenantid."' AND di_number='".$tn."' ORDER BY di_number";
	$di_dids_query = mysql_query($di_dids_sql) or die(mysql_error());
	$di_dids_row = mysql_fetch_array($di_dids_query, MYSQL_ASSOC);
	if(empty($di_dids_row)){
		$custom_validation_error = true;
		$status = 'error';
		$message = 'The Numbers field not valid.';
	}
	if(!$custom_validation_error){
		$params = array(
			'tn' => $tn,
			'name' => $name,
			'streetNum' => $streetNum,
			'streetInfo' => $streetInfo,
			'location' => $location,
			'city' => $city,
			'state' => $state,
			'postalCode' => $postalCode,
		);
		$url = $multitel_api_url.'v3/tn911';
		$curlopt = array(
			CURLOPT_RETURNTRANSFER => true,   // return web page
			CURLOPT_HEADER         => false,  // don't return headers
			CURLOPT_FOLLOWLOCATION => true,   // follow redirects
			CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
			CURLOPT_ENCODING       => "",     // handle compressed
			CURLOPT_USERAGENT      => "MirtaPBX", // name of client
			CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
			CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
			CURLOPT_TIMEOUT        => 120,    // time-out on response
			CURLOPT_POST           => true,
			CURLOPT_POSTFIELDS     => $params,
			CURLOPT_HTTPAUTH       => CURLAUTH_BASIC,
			CURLOPT_USERPWD        => "$multitel_username:$multitel_password", 
		);
		$ch = curl_init($url);
		curl_setopt_array($ch, $curlopt);
		$content  = curl_exec($ch);
		curl_close($ch);
		$tn911_result_obj = json_decode($content);
		//echo  __FILE__.'<br />Line : '.__LINE__.' <br /> var: <pre>'.print_r($tn911_result_obj, true).'</pre>';exit;
		if(!empty($tn911_result_obj->status->code) && $tn911_result_obj->status->code == 200){
			$di_emergencynotes = $streetNum.' '.$streetInfo.', '.$city.', '.$state.', '.$postalCode;
			if(!empty($di_dids_row['di_id'])){
				$di_dids_update_sql = "UPDATE di_dids SET di_allowemergency='on', di_emergencynotes='".$di_emergencynotes."' WHERE di_id='".$di_dids_row['di_id']."'";
				mysql_query($di_dids_update_sql);
			}
			$status = 'success';
			$message = 'Successfully Saved';
		} else {
			$status = 'error';
			$message = 'Failed, please try again.';
		}
		
	}
	$message_arr = array(
		'status' => $status,
		'message' => $message
	);
	echo json_encode($message_arr);
	exit;
}

$getareas_arr = listState();

$di_dids_sql = "SELECT * from di_dids WHERE di_te_id='".$tenantid."' AND di_number LIKE '1%' ORDER BY di_number";
$di_dids_query = mysql_query($di_dids_sql) or die(mysql_error());
$di_dids_arr = array();
while ($di_dids_row = mysql_fetch_assoc($di_dids_query)) {
	$di_dids_arr[] = $di_dids_row;
}
//echo  __FILE__.'<br />Line : '.__LINE__.' <br /> var: <pre>'.print_r($di_dids_arr, true).'</pre>';exit;
$smarty->assign('di_dids_arr',$di_dids_arr);
$smarty->assign('pagetitle', "E911");
$smarty->assign('getareas_arr', $getareas_arr);
$smarty->display('e911.tpl');

?>
