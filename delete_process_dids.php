<?php
/*
 * Copyright 2018-2019 MultiTEL LLC
 * @ Website    : https://www.multitel.net
 * @ Released	: 2019/04/27

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

 */

require_once("include/general.inc.php");
check_auth();

require_once 'include/db.inc.php';
require_once 'include/multitel.config.php';

if ($_SESSION['hasMenuConfiguration']=='') {
  $smarty->display('forbidden.tpl');
  die();
}
$tenant=$_SESSION['selectedTenant'];

$status ='';
$message ='';

if(isset($_POST)){
	$number = mysql_real_escape_string(trim($_POST['number']));
	$diid = mysql_real_escape_string($_POST['id']);
	
	$method = 'releasenumbers';
	
	$server_url = $json_url.$method;
	
	$data = array(
		'UserName' => $multitel_api_username,
		'Password' => $multitel_api_password,
		'Number' => $number,
	);
	
	$handle = curl_init($server_url);
	curl_setopt($handle, CURLOPT_POST, true);
	curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
	curl_exec($handle);
	
	$sql = "delete from di_dids where di_id=$diid and di_te_id='$tenant' and di_number='$number'";
	$res = $dbconn->Execute($sql);

	if($res){
		$status ='success';
		$message ='Success Delete Number #'.$number;
	} else {
		$status ='error';
		$message ='Failed Delete Number #'.$number;
	}
	
	$json = array(
		  'status' => $status,
		  'message' => $message,
	);
	$enc = json_encode($json);
	echo $enc;
}
?>
