<?php
/*
 * Copyright 2018-2019 MultiTEL LLC
 * @ Website    : https://www.multitel.net
 * @ Released   : 2019/04/27

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

require_once 'include/general.inc.php';
check_auth();

require_once 'include/db.inc.php';
require_once 'include/multitel.config.php';

$custom_validation_error = false;
$status_return = "error";
$message = "No numbers purchased";

$tenantid = ($_SESSION['selectedTenant']);

$share_tenantid = 0;
$multitel_username = get_multitel_setting($share_tenantid, 'MULTITELUSER');
$multitel_password = get_multitel_setting($share_tenantid, 'MULTITELPASSWORD');

$number = (!empty($_POST['number']) ? mysql_real_escape_string(trim($_POST['number'])) : '');


if(empty($number)){
	$custom_validation_error = true;
	$status_return = "error";
	$message = "Failed, number not found.";
}

if(!$custom_validation_error){
	$multitel_api_url = get_multitel_api_url();
	$version_api = get_version_api($share_tenantid);
	
	$url = $multitel_api_url.$version_api.'/release/'.$number;
	
	$curlopt = array(
		CURLOPT_RETURNTRANSFER => true,   // return web page
		CURLOPT_HEADER         => false,  // don't return headers
		CURLOPT_FOLLOWLOCATION => true,   // follow redirects
		CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
		CURLOPT_ENCODING       => "",     // handle compressed
		CURLOPT_USERAGENT      => "MirtaPBX", // name of client
		CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
		CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
		CURLOPT_TIMEOUT        => 120,    // time-out on response
		CURLOPT_POST           => true,
		CURLOPT_HTTPAUTH       => CURLAUTH_BASIC,
		CURLOPT_USERPWD        => "$multitel_username:$multitel_password",
	);
	
	$ch = curl_init($url);
	curl_setopt_array($ch, $curlopt);
	$content  = curl_exec($ch);
	curl_close($ch); print_r($content);
	$release_result_obj = json_decode($content);
	
	if(!empty($release_result_obj->status->code) && $release_result_obj->status->code == 200){
		if(!empty($_POST['release_number_confirm_pbx'])){
			$di_dids_delete_sql = "DELETE FROM di_dids WHERE di_number='".$number."'";
			$di_dids_delete_query = mysql_query($di_dids_delete_sql) or die(mysql_error());
		}
		$status_return = 'success';
		$message = 'Success release number #'.$number;
	} else {
		$status_return = 'error';
		$message = 'Failed, please try again.';
	}
}


$message_arr = array(
	'status' => $status_return,
	'message' => $message,
);
echo json_encode($message_arr);
?>
