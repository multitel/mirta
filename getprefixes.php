<?php
/*
 * Copyright 2018-2019 MultiTEL LLC
 * @ Website    : https://www.multitel.net
 * @ Released	: 2019/04/27

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

 */
require_once 'include/general.inc.php';
check_auth();

require_once 'include/multitel.config.php';

$uuid = (!empty($_GET['uuid']) ? $_GET['uuid'] : '');
$uuid = mysql_real_escape_string(trim($uuid));

$tenantid = 0;
$multitel_api_url = get_multitel_api_url();
$multitel_username = get_multitel_setting($tenantid, 'MULTITELUSER');
$multitel_password = get_multitel_setting($tenantid, 'MULTITELPASSWORD');
$version_api = get_version_api($tenantid);

$url = $multitel_api_url.$version_api.'/getprefixes/'.$uuid;
$curlopt = array(
	CURLOPT_RETURNTRANSFER => true,   // return web page
	CURLOPT_HEADER         => false,  // don't return headers
	CURLOPT_FOLLOWLOCATION => true,   // follow redirects
	CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
	CURLOPT_ENCODING       => "",     // handle compressed
	CURLOPT_USERAGENT      => "MirtaPBX", // name of client
	CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
	CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
	CURLOPT_TIMEOUT        => 120,    // time-out on response
	CURLOPT_POST           => true,
	CURLOPT_HTTPAUTH       => CURLAUTH_BASIC,
	CURLOPT_USERPWD        => "$multitel_username:$multitel_password", 
);

$ch = curl_init($url);
curl_setopt_array($ch, $curlopt);
$content  = curl_exec($ch);
curl_close($ch);
$getprefixes_result_obj = json_decode($content);

header('Content-Type: text/plain');

$getprefixes_html = "";
if(!empty($getprefixes_result_obj->response)){
	foreach ($getprefixes_result_obj->response as $getprefixes_obj) {
		$uuid = (!empty($getprefixes_obj->uuid) ? $getprefixes_obj->uuid : '');
		$needs_registration_html = "No";
		if(!empty($getprefixes_obj->needs_registration)){
			$show_registration_html = 'show_registration("'.$uuid.'")';
			$needs_registration_html = "
			<a href='javascript:;' onclick='".$show_registration_html."'>
				Yes
			</a>
			<div id='restrictions_".$uuid."' class='hidden'>
				".(!empty($getprefixes_obj->restrictions) ? nl2br($getprefixes_obj->restrictions) : '')."
			</div>
			";
		}
		$getprefixes_html .= "
			<tr>
			<td>".(!empty($getprefixes_obj->prefix) ? $getprefixes_obj->prefix : '')."</td>
			<td>".(!empty($getprefixes_obj->name) ? $getprefixes_obj->name : '')."</td>
			<td>".(!empty($getprefixes_obj->metered) ? $getprefixes_obj->metered : '')."</td>
			<td>".(!empty($getprefixes_obj->overflow) ? $getprefixes_obj->overflow : '')."</td>
			<td>".$needs_registration_html."
			</td>
			<td>
				<li class='cursor-pointer show_picknumbers btn btn-default btn-xs mb-5' alt='Show numbers' title='Show numbers' data-uuid='".$uuid."'>Show numbers</li>
			</td>
		</tr>
		";
	}
} else {
	$getprefixes_html = "
	<tr>
		<td colspan='6' class='text-center'>
			No matching records found
		</td>
	</tr>
	";
}
echo $getprefixes_html;
?>
